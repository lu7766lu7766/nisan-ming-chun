<?
session_start();
include("config/db_connect.php"); 

//+++++++++++++++可修改的參數+++++++++++++++++++

$show_num=5;//秀出資料筆數


//------------取得本頁的回傳值--------------------


$now_num = $_GET["now_num"];//取得分頁數字


//--------------分頁的語法_資料庫查詢所有的總筆數---------------
$sql_count = "SELECT COUNT(id) FROM data";
$result1 = mysql_query($sql_count);
$row1 = mysql_fetch_array($result1);
$total_num = $row1[0];//取出總筆數
//$total_num = 120;


$total_page = ceil( $total_num/$show_num );//分頁的語法_取出總數無條件進位

	if($now_num == "")//假如現在的頁碼=0或是空直給他判斷第一頁
	{
		$now_num=1;
	}
	
$showp = $now_num - 1;
$showp = $showp * $show_num;
	

?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="Description" content="專營建築水電材料製造">
<meta name="Description" content="銘浚興業有限公司以超越經營者角度的細膩思維，以最高品質為信念,讓所有音樂家能演奏出最美妙的音樂旋律。
樂器批發 , 樂器買賣 ,專業樂器維修 , 音樂事業合作,管弦樂器買賣，管弦樂器維修,銅管樂器,木管樂器">
<meta name="KeyWords" content="銘浚興業設立於雲林縣，專營建築水電材料製造，期能提供更好的服務、品質及低價格供應大眾。產品項目：PVC管零件製造、不銹鋼放衣架、立布生產、PVC銅珠球塞、各種水電材料批發、雲林PVC塑膠接頭零件系列、PVC銅珠凡而、PVC凡而、套銅外牙接頭零件、套銅龍口接頭系列、排水管帽系列、手工大月彎、PVC法蘭接頭、水塔接頭、ABS馬達架、不銹鋼立布、竹筏塞頭、TP-636活動放衣架">
<title>銘浚興業有限公司--最新消息</title>
<link rel="stylesheet" href="CSS/music_style.css" type="text/css">
<meta name="Author" content="FLYINGANGLE 飛角設計">
<link rev="made" href="fastudio268@gmail.com">
<link rev="made" href="http://www.fas-d.com/">

<meta property="og:description" content="銘浚興業有限公司以超越經營者角度的細膩思維，以最高品質為信念,讓所有音樂家能演奏出最美妙的音樂旋律。
樂器批發 , 樂器買賣 ,專業樂器維修 , 音樂事業合作,管弦樂器買賣，管弦樂器維修,銅管樂器,木管樂器" />
<meta property="og:title" content="銘浚興業有限公司---臺中古典樂器推手"/>
<meta property="og:type" content="website"/>
<meta property="og:url" content="http://www.fas-d.com/music/music.php"/>
<meta property="og:image" content="http://www.fas-d.com/music/images/index/index_bg.jpg"/>
<meta property="og:image" content="http://www.fas-d.com/music/images/about/about_bg.jpg"/>
<meta property="og:site_name" content="銘浚興業有限公司---臺中古典樂器推手" />

<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery.color.js"></script>
<script type="text/javascript" src="js/jquery.cycle.all.js"></script>
<link rel="stylesheet" href="CSS/music_style.css" type="text/css">

<style>
body , html
{
	overflow:auto;
}
</style>
<script type="text/javascript">


//擷取螢幕寬高	
function GetWebrowser_W_H(thisv)

{

var myWidth;
var myHeight;

if( typeof( window.innerWidth ) == 'number' ) { 

//用在不是IE的瀏覽器上

myWidth = window.innerWidth;
myHeight = window.innerHeight; 

} else if( document.documentElement && 

( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) { 

//用在IE6以上

myWidth = document.documentElement.clientWidth; 
myHeight = document.documentElement.clientHeight; 

} else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) { 

//相容IE4

myWidth = document.body.clientWidth; 
myHeight = document.body.clientHeight; 

}

/*
	if (thisv == 'nowh'){
		return myHeight;
	}else{
		return myWidth;
	}
*/	
	return myWidth + ':' + myHeight;

}



//取得滑鼠座標

<!--

// Detect if the browser is IE or not.
// If it is not IE, we assume that the browser is NS.
var IE = document.all?true:false

// If NS -- that is, !IE -- then set up for mouse capture
if (!IE) document.captureEvents(Event.MOUSEMOVE)

// Set-up to use getMouseXY function onMouseMove
document.onmousemove = getMouseXY;

// Temporary variables to hold mouse x-y pos.s
var tempX = 0
var tempY = 0

// Main function to retrieve mouse x-y pos.s

function getMouseXY(e) {

  if (IE) { // grab the x-y pos.s if browser is IE
    tempX = event.clientX + document.body.scrollLeft
    tempY = event.clientY + document.body.scrollTop
  } else {  // grab the x-y pos.s if browser is NS
    tempX = e.pageX
    tempY = e.pageY
  }  
  // catch possible negative values in NS4
  //alert(tempX + ":"+ tempY);
  if (tempX < 0){tempX = 0}
  if (tempY < 0){tempY = 0}  
  //alert(tempX);
  var getH = GetWebrowser_W_H();
  var getHA = getH.split(":");
  var geth = getHA[1];
  var getw = getHA[0];
  // alert(getH);
  var getHalf = geth / 2;
  
  if(getH > getHalf ){
	  var main_bgx = tempX / -100 ;
	  var news_area = (tempX / 200)+100 ;
	  var news_main = (tempX / 200);
  }
  else{
	  var main_bgx = tempX / 100 ;
	  var news_area = (tempX / -200)+100;
	  var news_main = (tempX / 200);
  }
  
  $('.main_news_img').css('left', main_bgx);
  $('#news_area').css('left', news_area);
  $('#news_main').css('left', news_main);
  
  // show the position values in the form named Show
  // in the text fields named MouseX and MouseY
  document.Show.MouseX.value = tempX
  document.Show.MouseY.value = tempY
  

  
  return true
}




$(document).ready(function() {

	/*
	$('.ch1').mouseover(function(){
		
		$('.ch1_title').stop().animate({color:'#FFF'},tit_speed);
		$('.ch1_english').stop().animate({color:'#FFF'},eng_speed);
		$('.ch1_h').stop().animate({top:"-21px"}, 300);

		
	});
	$('.ch1').mouseout(function(){
		$('.ch1_title').stop().animate({color:'#000'},tit_speed);
		$('.ch1_english').stop().animate({color:'#000'},eng_speed);
		$('.ch1_h').stop().animate({top:"279px"}, 300);
	});
	*/
	//$('#ch1_cc').addClass('animated fadeInUp');
	
	//首頁動畫把PX轉到0
	$('.news_line').stop().animate({width:"0px"}, 0);
	$('.news_txt1').stop().animate({width:"0px"}, 0);
	$('.news_txt2').stop().animate({width:"0px"}, 0);

	
	//首頁選單把PX轉到0
	/*$('.ch1_h').stop().animate({width:"0px"}, 0);
	$('.ch2_h').stop().animate({width:"0px"}, 0);
	$('.ch3_h').stop().animate({width:"0px"}, 0);
	$('.ch4_h').stop().animate({width:"0px"}, 0);
	$('.ch5_h').stop().animate({width:"0px"}, 0);*/
	
	//$('.main_txt1').stop().animate({width:"162px"}, 1000);
	setTimeout("news_line()",1000);
	
}); 

function news_line()
{
	//alert();
	$('.news_line').stop().animate({width:"100px"}, 1000);	
	setTimeout("news_txt1()",1000);
}

function news_txt1()
{
	$('.news_txt1').stop().animate({width:"105px"}, 1000);	
	setTimeout("news_txt2()",1000);
}

function news_txt2()
{
	$('.news_txt2').stop().animate({width:"233px"}, 1000);	
	//setTimeout("about_txt3()",1000);
}
/*
function about_txt3()
{
	$('.about_txt3').stop().animate({width:"152px"}, 1000);
}*/


</script>

</head>

<body>
 <div style="z-index:-2" class="main_news_img"></div>
<div class="top_fixed">
	<? include("music_part/top.php");?>
</div>

<!--oooooooooooooooooooooooooooooooo-->
<div id="main_news1">
    
    <div class="news_line main_level"></div>
    
    <div id = "news_main">
        <div class="news_txt1 main_level"></div>
        <div class="news_txt2 main_level"></div>
    </div>
    <div id = "news_area" >
  
<?php 
if ($now_num <> '1'){
$nowpgsu = $now_num - 1;
	$FSTR = "music_news.php?now_num=".$nowpgsu;
}else{
	$FSTR = "#";
}

if ($now_num <> $total_page){
$nowpgsun = $now_num + 1;
	$NSTR = "music_news.php?now_num=".$nowpgsun;
}else{
	$NSTR = "#";
}
		//--------------資料庫查詢所有的日期以及撈幾筆到幾筆LIMIT---------------
		$sql = "SELECT * FROM data order by id desc LIMIT ".$showp.",".$show_num;
		$result = mysql_query($sql); //得到查詢結果數據集
	
		//撈資料庫的資料
		while( $row = mysql_fetch_array($result) ){
			$title		=	$row['title'];//抓取標題
			$img		=	$row['PIMGS01'];//抓取圖片
			$user		=	$row['user']; //抓取使用者
			$postdate	=	$row['postdate'];//抓取日期
			$f_id		=	$row['id'];//抓取流水號
			$content 	=	$row['content'];
			$conuter 	=	$row['conuter'];
			$content 	= 	ereg_replace("<[^>]*>", "", $content);//過濾HTML符號
			
			
/*			//PHP取出字元數可自訂!!
			$a = strip_tags($content); //去除HTML標籤
			$sub_content = mb_substr($a, 0,180, 'UTF-8'); //擷取字串*/
			$subcontent = mb_substr($content,0,100,'utf8');
			
			$PIMGS01str  = explode("!#!", $img);      
			for ($ii = 0; $ii <= sizeof($PIMGS01str); $ii++){
				
				$PIMGS01strs  = explode(";", $PIMGS01str[$ii]);
					$MDimgID = 'MDimgID'.$ii;
					$MDstrID = 'MDstrID'.$ii;
					$MDurlID = 'MDurlID'.$ii;	
				$$MDimgID = $PIMGS01strs[0];//圖檔
				$$MDstrID = $PIMGS01strs[1];//說明
				$$MDurlID = $PIMGS01strs[2];//連結								
			}
			
?>     
    
    	<div class="news_part">
            <div class="news_img"><img src="sys/MainAP/sys_news/<? echo $MDimgID1; ?>" width="130" height="160"></div>
            <div class="news_right">
                <div class="news_title"><? echo $title; ?></div>
                <div class="news_content"><? echo $subcontent; ?></div>
              <a href="music_news_txt.php?f_id=<? echo $f_id; ?>">  <div class="news_more">more...</div></a>
                <div class="news_part_line"></div>
            </div>
        </div>    
        
<?php 	
	}
	$row = mysql_fetch_array($result);
	//撈資料庫的資料結束
?>   
    
<!--        
    	<div class="news_part">
            <div class="news_img"><img src="images/index/index_house.png" width="145" height="160"></div>
            <div class="news_right">
                <div class="news_title">│新產品上架公告│</div>
                <div class="news_content">「藏美」的智慧深具品味，退居繁華的幕後靜巷裡，與中山高速公路維持剛好隔絕噪音和落塵的微距離，保有、淨、近的美麗境界。</div>
                <div class="news_more">more...</div>
                <div class="news_part_line"></div>
            </div>
        </div>  
    	<div class="news_part">
            <div class="news_img"><img src="images/index/index_house.png" width="145" height="160"></div>
            <div class="news_right">
                <div class="news_title">│新產品上架公告│</div>
                <div class="news_content">「藏美」的智慧深具品味，退居繁華的幕後靜巷裡，與中山高速公路維持剛好隔絕噪音和落塵的微距離，保有、淨、近的美麗境界。</div>
                <div class="news_more">more...</div>
                <div class="news_part_line"></div>
            </div>
        </div>  -->

        <div style="font-size:13px; position:absolute; top:140px; left:650px; z-index:9999999;">
            <div style="float:left; background:URL(images/news/arrow_l.png) no-repeat; margin-top:3px;"><a href="<? echo $FSTR; ?>"><img src="images/mask.gif" width="10" height="18"></a> </div>
            <div style="float:left; margin-left:3px;"> <? echo $now_num; ?> </div>
            <div style="float:left; margin-left:3px;"> of </div>
            <div style="float:left; margin-left:3px;"> <? echo $total_page;  ?> </div>
            <div style="float:left; background:URL(images/news/arrow_r.png) no-repeat; margin:3px 0 0 2px;"><a href="<? echo $NSTR; ?>"><img src="images/mask.gif" width="10" height="18"></a></div>
   	    </div>  
        
    </div>

</div>



<!--oooooooooooooooooooooooooooooooo-->

<div class="lig_backdrop"></div>
<div class="box" id="targetdiv"></div>
<div class="box" id="targetdiv2"></div>
<div style="position:fixed; z-index:9999; bottom:0px; left:0;">
	<? include("music_part/footer.php");?>
</div>
</body>
</html>