jQuery.cookie = function (a, b, c) {
    if (typeof b != 'undefined') {
        c = c || {};
        if (b === null) {
            b = '';
            c.expires = -1
        }
        var d = '';
        if (c.expires && (typeof c.expires == 'number' || c.expires.toUTCString)) {
            var e;
            if (typeof c.expires == 'number') {
                e = new Date();
                e.setTime(e.getTime() + (c.expires * 24 * 60 * 60 * 1000))
            } else {
                e = c.expires
            }
            d = '; expires=' + e.toUTCString()
        }
        var f = c.path ? '; path=' + (c.path) : '';
        var g = c.domain ? '; domain=' + (c.domain) : '';
        var h = c.secure ? '; secure' : '';document.cookie = [a, '=', encodeURIComponent(b), d, f, g, h].join('')
    } else {
        var j = null;
        if (document.cookie && document.cookie != '') {
            var k = document.cookie.split(';');
            for (var i = 0; i < k.length; i++) {
                var l = jQuery.trim(k[i]);
                if (l.substring(0, a.length + 1) == (a + '=')) {
                    j = decodeURIComponent(l.substring(a.length + 1));
                    break
                }
            }
        }
        return j
    }
};
(function (d) {
    d.each(['backgroundColor', 'borderBottomColor', 'borderLeftColor', 'borderRightColor', 'borderTopColor', 'color', 'outlineColor'], function (i, b) {
        d.fx.step[b] = function (a) {
            if (a.state == 0) {
                a.start = getColor(a.elem, b);
                a.end = getRGB(a.end)
            }
            a.elem.style[b] = "rgb(" + [Math.max(Math.min(parseInt((a.pos * (a.end[0] - a.start[0])) + a.start[0]), 255), 0), Math.max(Math.min(parseInt((a.pos * (a.end[1] - a.start[1])) + a.start[1]), 255), 0), Math.max(Math.min(parseInt((a.pos * (a.end[2] - a.start[2])) + a.start[2]), 255), 0)].join(",") + ")"
        }
    });

    function getRGB(a) {
        var b;
        if (a && a.constructor == Array && a.length == 3) return a;
        if (b = /rgb\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*\)/.exec(a)) return [parseInt(b[1]), parseInt(b[2]), parseInt(b[3])];
        if (b = /rgb\(\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*\)/.exec(a)) return [parseFloat(b[1]) * 2.55, parseFloat(b[2]) * 2.55, parseFloat(b[3]) * 2.55];
        if (b = /#([a-fA-F0-9]{2})([a-fA-F0-9]{2})([a-fA-F0-9]{2})/.exec(a)) return [parseInt(b[1], 16), parseInt(b[2], 16), parseInt(b[3], 16)];
        if (b = /#([a-fA-F0-9])([a-fA-F0-9])([a-fA-F0-9])/.exec(a)) return [parseInt(b[1] + b[1], 16), parseInt(b[2] + b[2], 16), parseInt(b[3] + b[3], 16)];
        return e[d.trim(a).toLowerCase()]
    }
    function getColor(a, b) {
        var c;
        do {
            c = d.curCSS(a, b);
            if (c != '' && c != 'transparent' || d.nodeName(a, "body")) break;
            b = "backgroundColor"
        } while (a = a.parentNode);
        return getRGB(c)
    };
    var e = {
        aqua: [0, 255, 255],
        azure: [240, 255, 255],
        beige: [245, 245, 220],
        black: [0, 0, 0],
        blue: [0, 0, 255],
        brown: [165, 42, 42],
        cyan: [0, 255, 255],
        darkblue: [0, 0, 139],
        darkcyan: [0, 139, 139],
        darkgrey: [169, 169, 169],
        darkgreen: [0, 100, 0],
        darkkhaki: [189, 183, 107],
        darkmagenta: [139, 0, 139],
        darkolivegreen: [85, 107, 47],
        darkorange: [255, 140, 0],
        darkorchid: [153, 50, 204],
        darkred: [139, 0, 0],
        darksalmon: [233, 150, 122],
        darkviolet: [148, 0, 211],
        fuchsia: [255, 0, 255],
        gold: [255, 215, 0],
        green: [0, 128, 0],
        indigo: [75, 0, 130],
        khaki: [240, 230, 140],
        lightblue: [173, 216, 230],
        lightcyan: [224, 255, 255],
        lightgreen: [144, 238, 144],
        lightgrey: [211, 211, 211],
        lightpink: [255, 182, 193],
        lightyellow: [255, 255, 224],
        lime: [0, 255, 0],
        magenta: [255, 0, 255],
        maroon: [128, 0, 0],
        navy: [0, 0, 128],
        olive: [128, 128, 0],
        orange: [255, 165, 0],
        pink: [255, 192, 203],
        purple: [128, 0, 128],
        violet: [128, 0, 128],
        red: [255, 0, 0],
        silver: [192, 192, 192],
        white: [255, 255, 255],
        yellow: [255, 255, 0]
    }
})(jQuery);
(function ($) {
    $.fn.stickyBar = function (c) {
        var c = $.extend({
            backgroundColor: '#292929',
            closeButtonText: 'Hide Me',
            color: 'white',
            hoverBG: '#3e3e3e',
            imagesFolderPath: 'images',
            pos: 'bottom',
            displayHideButton: true
        }, c);
        return this.each(function () {
            var b = $(this),
                listItems = b.find('li'),
                liAnchors = $(listItems).children('a'),
                subUL = $(listItems).children('ul, ol, dl')[0] ? $(listItems).children('ul, ol, dl') : null;b.wrap('<div></div>').parent('div').append('<span class="openButton"></span>');
            if ($.cookie('bottomBar')) {
                b.hide()
            }
            $('.openButton').css({
                background: c.backgroundColor + ' url(' + c.imagesFolderPath + '/openButton.png) no-repeat 8px 2px'
            });
			$.cookie('bottomBar') ? $('.openButton').css('bottom', 0) : $('.openButton').css('bottom', b.height());
			b.css({
                'background': c.backgroundColor,
                'zIndex': 9999999998
            });$('li').hover(function () {
                if ($(this).children('ul, ol, dl')[0]) {
                    var a = $(this).children('ul, ol, dl').eq(0).height();
                    if (c.pos === 'bottom') {
                        if (subUL) {
                            $(subUL).css('top', '-' + a + 'px')
                        }
                    } else if (c.pos === 'top') {
                        if (subUL) {
                            $(subUL).css('top', b.height() + 'px')
                        }
                    }
                }
            });
            if (c.color) {
                $(liAnchors).css({
                    'color': c.color,
                    'textDecoration': 'none'
                })
            }
            if (c.pos === 'top') {
                b.css('top', '0')
            }
            $(listItems).css('background', c.backgroundColor).hover(function () {
                $(this).stop(true, false).animate({
                    backgroundColor: c.hoverBG
                }, 200).css('cursor', 'pointer');
                if ($(this).children()[0]) {
                    $(this).children('ul, ol, dl').hide().fadeIn(200)
                }
            }, function () {
                $(this).stop(true, false).animate({
                    backgroundColor: c.backgroundColor
                }, 200).children('ul, ol, dl').hide()
            });$('.openButton').hover(function () {
                $(this).css('background-color', c.hoverBG)
            }, function () {
                $(this).css('background-color', c.backgroundColor)
            });$('.openButton').live('click', function () {
                closeBar(b)
            })
        });

        function closeBar(a) {
            $.cookie('bottomBar') ? $('.openButton').animate({
                bottom: '50px'
            }, 850, 'swing') : $('.openButton').animate({
                bottom: 0
            }, 850, 'swing');a.animate({
                'height': 'toggle'
            }, 850, 'swing', function () {
                $.cookie('bottomBar') ? $.cookie('bottomBar', null) : $.cookie("bottomBar", "closed")
            })
        }
    }
})(jQuery);