# jQuery MSG Plugin CHANGELOG

## 1.0.2

* Update PIE.htc for IE9

## 1.0.1

* Cancel preloading background image

## 1.0.0

* First stable release