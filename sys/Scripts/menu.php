<!--<script src="http://code.jquery.com/jquery-1.4.2.min.js" language="javascript"></script>-->
<script language="javascript">
	$(function(){
		$(document).click(function(){
			$("#minwt_mainMenu").css('display','none');
		});
		$("#minwt_mainMenu li").each(function(){
			var $this = $(this);
			//找尋#minwt_mainMenu li中的ul標籤是否>0
			if($this.find("ul").length>0){
				$(this).children("a").append("<img src='images2/arrow.png' class='img'>");
			}
		});
 
		$("#minwt_startBtn a").click(function(){
			if($("#minwt_mainMenu").is(":visible")){
				$("#minwt_mainMenu").slideUp();
			}else{
				$("#minwt_mainMenu").slideDown();
				$("#minwt_mainMenu ul").hide();
			}	
 
			return false;
		});
 
		$("#minwt_mainMenu li a").click(function(){				
			var $this= $(this),
				$parent = $this.parent(),
				$next = $this.next("ul"),
				_idx = $parent.index();//取得索引
 
			if($next.length>0){
				if($next.is(":visible")){//判斷顯示與否
					$next.slideUp();
				}else{
					//先將父層兄弟的ul標籤關閉
					$parent.siblings().find("ul").slideUp();
					$next.css('top',55*_idx).slideDown();
				}
				//關閉連結
				return false;
			}
		});			
 
 
		//消除連結虛線框
		$("a").focus( function(){
			$(this).blur();
		});
 
	});
</script>