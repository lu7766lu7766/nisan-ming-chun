<?
session_start();
require_once('../config/KEY_SESSION.php'); 
//header("Cache-Control: no-cache, must-revalidate");

$LoginUserID = $_SESSION["LoginUserID"];//登入管理之帳號
if ($LoginUserID == ''){
	$checktogo = "top.location.href='../Logout.php';";
	echo "<script Language='JavaScript'>";
	echo "alert('登入逾時，或未登入異常，請重新登入！ ');";
	//echo $checktogo;
	echo "</script>";
	}

function MMDATES($TodayStr,$MM){

	$MY_Y = substr ($TodayStr, 0 ,4);
	$MY_m = substr ($TodayStr, 5 ,2);
	$MY_d = substr ($TodayStr, 8 ,2);

	$SMY_Y = $MY_Y;
	
	$SM = $MY_m - $MM;
		
	if ($SM == 0){
		$SMY_Y = $SMY_Y - 1;
		$SM = '12';
		}

	if ($SM == -1){
		$SMY_Y = $SMY_Y - 1;
		$SM = '11';
		}
		
	if ($SM <= 9){
		$SM = '0'.$SM;
		}
	
	$StartCDate = $SMY_Y.'/'.$SM.'/05';	
	$dayCount = date("t",strtotime($StartCDate));   //本月共有幾天	
	$EndCDate	= $SMY_Y.'/'.$SM.'/'.$dayCount;	
	return $StartCDate;//回傳開始日期:結算日期
	}

$SDATE = MMDATES($MYDATE,'1');//結算日期	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>獎金結算作業</title>

		<link type="text/css" href="nCSS/css/start/jquery-ui-1.8.18.custom.css" rel="stylesheet" />
		
		<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>
        
	<script src="../Scripts/jquery.ui.datepicker.js"></script> 
	<script src="../Scripts/jquery.ui.datepicker-zh-TW.js"></script> 
    
<script type="text/javascript">
		$(function() {

			var dates = $( "#SDATE, #EDATE" ).datepicker({
					beforeShowDay: function(date){ 
					  //var day = date.getDay();//getMonth
					 // return [day == 1 || day == 4,""];
					  //return [day == 5 ,""];
					  
						var day = date.getDay();
						//var weeks='1#2#3';   //周日为0
						//var b=weeks.indexOf(day);
						var days = '1,2,3,4,5,6,0';   //周日为0
						var b=days.indexOf(day);
							return [(b>=0), ''];					  
					  
					},
				dateFormat : 'yy/mm/dd',
				//defaultDate: "+1w",
				changeMonth: true,
				//numberOfMonths: 3,
				changeMonth: true,
				//changeYear: true,
				onSelect: function( selectedDate ) {
					SEDATE_Keying();
					var option = this.id == "SDATE" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" ),
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
					dates.not( this ).datepicker( "option", option, date );
				}
			});
			
			
			
			$( "#STEP1" ).buttonset();
		});




//=============================================================
	function Settlement(){
			$("#ShowLoading").show();
			var LoginUserID = '<? echo $LoginUserID;?>';
			//var LoginUserID = 'admin';
			var SDATE = $("#SDATE").val();
			var thisK = $("#thisK").val();
			
			if ( $("#todb").attr('checked') ) {
					var todb = '1';
				}else{
					var todb = '0';
				}


			$.ajax({
			url:"../function/SettlementV1.php",
			type:"POST",
			dataType: "text",
			data:{LoginUserID:LoginUserID,SDATE:SDATE,todb:todb,thisK:thisK},
			success:function(msg){
				var thismsg = msg;
				//alert(thismsg);
				if(thismsg != 0 && thismsg != 1 ){
						$("#ShowLoading").hide();
						$("#nowrun").html('0');
						$("#totlerun").html('0');
						$("#ShowDetail").html(msg);
						}
						
				if(thismsg == 1 ){
						alert('無符合結算資料');
						$("#ShowLoading").hide();
						}						
						
				if(thismsg == 0){
						$("#ShowDetail").html('');
						$("#SDATE").val('');
						$("#ShowLoading").hide();
						alert('不明錯誤');
					}
			}
		});
	}
//=============================================================
function showtotlerun(thisv){
	$("#totlerun").html(thisv);
}

function shownowrun(thisv){
	$("#nowrun").html(thisv);
}
</script>
</head>


<div  align="center">
<body>
<input name="SDATE" id="SDATE" class="dateinputcss"  type="text" size="8" value="<? echo $SDATE;?>" onchange="SEDATE_Keying()"  />
,平衡獎金K值：<input name="thisK" id="thisK" type="text" size="2" value="1.00"  />
, 執行寫入 <input name="todb" id="todb" type="checkbox" value="1" /><br />
<input type="button" value="確認，進行下一步" onclick="Settlement()"  />
<hr />
<div id="ShowLoading" style="display: none">
<span><img src="../images/loadingAnimation.gif" width="208" height="13"  /></span>
<span id='nowrun' style='display:none'>0</span><span style='display:none'>/</span>
<span id='totlerun' style='display:none'>0</span>
</div>

<div id="ShowDetail" align="left" style="width:1500px" ></div>

</div>
</body>
</html>