<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script type="text/javascript" src="http://files.cnblogs.com/Zjmainstay/jquery-1.6.2.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="./ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="./ueditor/ueditor.all.js"></script>
<title>銘浚興業有限公司---後台系統</title>
<link href="ckeditor/_samples/sample.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="CSS/mem_table.css" />
<style>
#feedback{width:100px;}
#feedback img{float:left;width:200px;height:150px;}
#ZjmainstaySignaturePicture,#addpicContainer{float:left;width: 200px;}
#ZjmainstaySignaturePicture img{width: 100px;}
#addpicContainer img{float: left;}
.loading{display:none;float: left;}

.fancytable td{ text-align:left;}
</style>

<script type="text/javascript">
$(document).ready(function(){
	//响应文件添加成功事件
	$("#inputfile").change(function(){
		//创建FormData对象
		var data = new FormData();
		//为FormData对象添加数据
		$.each($('#inputfile')[0].files, function(i, file) {
			data.append('upload_file'+i, file);
		});
		$(".loading").show();	//显示加载图片
		//发送数据
		$.ajax({
			url:'submit_form_process.php',
			type:'POST',
			data:data,
			cache: false,
			contentType: false,		//不可缺参数
			processData: false,		//不可缺参数
			success:function(data){
				data = $(data).html();
				
				var splits = data.split("'");
				$("#pic").val(splits[1]);
				//alert(splits[1]);
				if($("#feedback").children('img').length == 0) $("#feedback").append(data.replace(/&lt;/g,'<').replace(/&gt;/g,'>'));
				else $("#feedback").children('img').eq(0).before(data.replace(/&lt;/g,'<').replace(/&gt;/g,'>'));
				$(".loading").hide();	//加载成功移除加载图片
			},
			error:function(){
				alert('上傳出錯');
				$(".loading").hide();	//加载失败移除加载图片
			}
		});
	});
});
</script>
</head>

<body>
<div id="mystickytooltip" class="stickytooltip">
<div style="padding:1px">
</div></div>


<?php 

include("connections/news_db.php");

?>
<form action="post_edit.php" method="post">
<div class="sexyborder" style="margin-left:23%; width:540px;">
<input style="display:none" type="text" name="pic" id="pic">
<table class="fancytable" border="1">
  <tr>
    <td colspan="3"><h1>銘浚興業有限公司---後台系統</h1>
    <? 
		echo '<a href="select.php">查詢</a>&nbsp;&nbsp;';
	?>
    </td>
  </tr>
  <tr class="headerrow">
    <td colspan="2">標題：
   		 <input name="title" type="text" id="title" size="60" />
    </td>

  </tr>
  
  <tr>
    <td>
    <div id="addpicContainer">
      <input type="file" multiple="multiple" name="inputfile" id="inputfile" />
      <div id="feedback"></div>	
      <!-- 响应返回数据容器 -->
      <span class="loading"></span>
    </div>
    </td>
    <td>
        類別：<label for="type3"></label>
        <input type="text" name="type" id="type" />
    </td>
  </tr>
  
  <tr>
    <td colspan="3">
      <div style="width:540px;">
		<textarea name="editor1" id="myEditor">這裡寫你的初始化內容</textarea>
       </div>
	</td>
  </tr>
  
  <tr>
      <td>
        <input style="margin-left:70%;" type="submit" value="送出"/>
      </td>
  </tr>

</table>
</div>
  <input name="postdate" type="hidden" id="postdate" value="<?php  echo DATE("Y/m/d");  ?>" />
        
<script type="text/javascript">
 window.UEDITOR_HOME_URL = "/sys/MainAP/news/ueditor/"; 

var editor = UE.getEditor('myEditor');

</script>
		
        
</form>

</body>
</html>