<?
session_start();
require_once('../config/KEY_SESSION.php'); 
require_once('../config/SYS_Function.php'); 
$titlestr = '基礎清潔保養';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Flexigrid</title>
<link rel="stylesheet" type="text/css" href="../Tables/css/flexigrid.css">
<script type="text/javascript" src="../Tables/js/jquery-1.5.2.min.js"></script>
<script type="text/javascript" src="../Tables/js/flexigrid.js"></script>
<script type="text/javascript">

</script>
<style type="text/css">
body {
	margin-left: 5px;
}
</style>
</head>
<body>
        
<table id="flex1" style="display:none;"></table>
<input id="ActionItem" type="hidden" name="hidden" value="null" />

<script type="text/javascript">
$("#flex1").flexigrid({
        url: '../Tables/P01_Xml.php',
        dataType: 'xml',
        colModel : [
				{display: '系統編號', 	name : 'PID', width : 70, sortable : true, align: 'center'},
				{display: '<? echo $titlestr;?>標題', 		name : 'title1', width : 395, sortable : true, align: 'left'},
                {display: '上架日期', 	name : 'CR_DATE', width : 120, sortable : true, align: 'left'},
				{display: '上架', 		name : 'MD_ShowYN', width : 40, sortable : true, align: 'left'},
                ],
			searchitems : [
				{display: '<? echo $titlestr;?>標題', name : 'title1', isdefault: true},
				{display: '上架日期', name : 'CR_DATE', isdefault: true}
				],
			buttons : [
			{name: '新增', bclass: 'ADD', onpress : button},
				<!--{name: '刪除', bclass: 'delete', onpress : button},-->
				{name: '修改', bclass: 'modify', onpress : button},				
				{separator: true}
				],				
        sortname: "PID",
		method:'POST', 
        sortorder: "asc",
        usepager: true,
        //title: '使用者列表',
        useRp: true,
        rp: 10,
		rpOptions:[10],
        //showTableToggleBtn: true,
        width: 710,
        //onSubmit: addFormData,
		nomsg: '無資料',
		procmsg: '資料處理中，請稍候 …',
		autoload: true,  
        height: 295
});



			function button(com,grid)
			{
				if (com=='刪除')
					{
						//${"#ActionItem"}.val("delete");
						if($('.trSelected',grid).length==0){
							//alert("请选择要删除的数据");
							top.AlertMsg('請選擇要刪除的<? echo $titlestr;?>。',200,30);//訊息Alert
						}else{
							var id ="";
								for(var i=0;i<$('.trSelected',grid).length;i++){
									if(i==$('.trSelected',grid).length-1){
										id += ","+$('.trSelected',grid).find("td:first").eq(i).text();
										//id += ""+$('.trSelected',grid).find("td:first").eq(i).text();
								} else {
										id += ","+$('.trSelected',grid).find("td:first").eq(i).text()+",";
										//id += "&"+$('.trSelected',grid).find("td:first").eq(i).text()+"&";
									}
								}
							
							var CStr = '確定要刪除此 ' + $(".trSelected",grid).length + ' 筆記錄嗎？<br>(該文章將永久刪除)';
							//top.AlertMsg(CStr,200,30);
							//reloadTable();
							var RUNPAGE = 'function/DelPDs.php?ID=' + id;
							top.ConfirmMsg(CStr,RUNPAGE,300,80,'PDListIFS');
							$('#flex1').flexOptions({}).flexReload();
							/* 
							if (top.ConfirmMsg(CStr,id,200,80)){
									alert('ok');
								}else{
									alert('no');
								}//訊息Alert
							*/
								//$('#flex1').flexOptions({}).flexReload();
							/*
							if(confirm('確定要刪除此 ' + $('.trSelected',grid).length + ' 筆記錄嗎？'))
							{
							 var  id ="";
						     for(var i=0;i<$('.trSelected',grid).length;i++){
						     	if(i==$('.trSelected',grid).length-1){
						     		id += "id="+$('.trSelected',grid).find("td:first").eq(i).text();
						     	} else {
						     		id += "id="+$('.trSelected',grid).find("td:first").eq(i).text()+"&";
						        }
						      }
							  alert(id);
						      //window.location.href="../ReleaseInfoServlet?hidden="+${"hidden"}.value+"&"+id;
					      }
						  */
					   }
					}
					
				else if (com=='新增')
					{
						//${"hidden"}.value="add";
						//window.location.href="infoAdd.jsp?hidden="+${"hidden"}.value;
					var url2go ="MainAP/Edit_Page_P01.php?MODETYPE=1&IFS=VIPCList1&THISM_ID=" + fixID + "&TABLENAME=PAGE_VIP01";
					top.AddIFInfo('新增<? echo $titlestr;?>','VIPCList1','700','500',url2go,'1');

					}
					
				else if (com=='修改')
				{
					//${"hidden"}.value="modify";
					if($(".trSelected").length==1){			
					//var thisv = $('.trSelected',grid).find("td").eq(2).text();
					//alert(thisv);

					var fixID = $('.trSelected',grid).find("td").eq(0).text();
					//self.location.href="SUserInfo.php?MODETYPE=2&BID=" + $('.trSelected',grid).find("td").eq(2).text() + "&ID=" + $('.trSelected',grid).find("td").eq(0).text();
					var url2go ="MainAP/Edit_Page_P01.php?MODETYPE=2&IFS=VIPCList1&THISM_ID=" + fixID + "&TABLENAME=PAGE_VIP01";
					//var url2go ="MainAP/Add_AD.php?MODETYPE=2&IFS=<? echo $IFS;?>&THISM_ID=" + fixID + "&TABLENAME=AD_Modes&KEYID=MD_ID&CTID=";
					top.AddIFInfo('編輯<? echo $titlestr;?>','VIPCList1','700','500',url2go,'1');


					
					}else if($(".trSelected").length>1){
						//alert("請一次選擇一位管理者進行編輯");
						top.AlertMsg('請一次選擇一筆<? echo $titlestr;?>進行編輯。',250,30);//訊息Alert
					}else if($(".trSelected").length==0){
						//alert("請先選擇要修改的管理者");
						top.AlertMsg('請先選擇要修改的<? echo $titlestr;?>。',220,30);//訊息Alert
					}
				}
			}  


function reloadTable1(){
	alert('reload1');
	//$('#flex1').flexOptions({}).flexReload();
	}
</script>

</body>
</html>