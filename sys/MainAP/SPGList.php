<?
session_start();
require_once('../config/SYS_Function.php'); 

$IFS 		= $_GET["IFS"];
$GETPTYPE	= $_GET["GETPTYPE"];
$TARGETID 	= '產品';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Flexigrid</title>
<link rel="stylesheet" type="text/css" href="../Tables/css/flexigrid.css">
<script type="text/javascript" src="../Tables/js/jquery-1.5.2.min.js"></script>
<script type="text/javascript" src="../Tables/js/flexigrid.js"></script>

<style type="text/css">
body {
	margin-left: 5px;
}
</style>
</head>
<body>
<? 

if ($GETPTYPE <> 'GP'){
	$STR = 'PG_Xml';
}else{
	$STR = 'PGGP_Xml';
}
?>
<table id="flex1" style="display:none;"></table>
<input id="ActionItem" type="hidden" name="hidden" value="null" />
<script type="text/javascript">
$("#flex1").flexigrid({
        url: '../Tables/<? echo $STR;?>.php', 
        dataType: 'xml',
        colModel : [
{display: '系統編號', 	name : 'PGNO', width : 40, sortable : true, align: 'center',hide: true},
{display: '方案名稱', 	name : 'ProjectName', width : 380, sortable : true, align: 'left'},
<?
if ($GETPTYPE == 'GP'){
?>
{display: 'GP', 		name : 'ProjectGP', width : 100, sortable : true, align: 'right'},
<?
}else{
?>
{display: '金額', 		name : 'ProjectPrice', width : 100, sortable : true, align: 'right'},
<?
}
?>
{display: 'PV點數', 		name : 'ProjectPV', width : 100, sortable : true, align: 'right'},
{display: '狀態', 		name : 'showyn', width : 30, sortable : true, align: 'left'}
                ],
			searchitems : [
				{display: '方案名稱', name : 'ProjectName', isdefault: true},
				{display: '金額', name : 'ProjectPrice'},
				{display: 'PV點數', name : 'ProjectPV'}
				],
			buttons : [
				{name: '刪除', bclass: 'delete', onpress : button},
				{name: '修改', bclass: 'modify', onpress : button},				
				{separator: true}
				],				
        sortname: "PGNO",
		method:'POST', 
        sortorder: "asc",
        usepager: true,
        //title: '使用者列表',
        useRp: true,
        rp: 10,
		rpOptions:[10],
        //showTableToggleBtn: true,
        width: 680,
        //onSubmit: addFormData,
		nomsg: '無資料',
		procmsg: '資料處理中，請稍候 …',
		autoload: true,  
        height: 295
});



			function button(com,grid)
			{
				if (com=='刪除')
					{
						//${"#ActionItem"}.val("delete");
						if($('.trSelected',grid).length==0){
							//alert("请选择要删除的数据");
							top.AlertMsg('請選擇要刪除的資料。',200,30);//訊息Alert
						}else{
							var id ="";
								for(var i=0;i<$('.trSelected',grid).length;i++){
									if(i==$('.trSelected',grid).length-1){
										id += ","+$('.trSelected',grid).find("td:first").eq(i).text();
										//id += ""+$('.trSelected',grid).find("td:first").eq(i).text();
								} else {
										id += ","+$('.trSelected',grid).find("td:first").eq(i).text()+",";
										//id += "&"+$('.trSelected',grid).find("td:first").eq(i).text()+"&";
									}
								}
							
							var CStr = '確定要刪除此 ' + $(".trSelected",grid).length + ' 筆記錄嗎？<br>(該資料將永久刪除)';
							//top.AlertMsg(CStr,200,30);
							//reloadTable();
						var RUNPAGE = 'function/DelTable.php?STR=PGList@PGNO@' + id ;
							top.ConfirmMsg(CStr,RUNPAGE,300,80,'<? echo $IFS;?>IFS');
							$('#flex1').flexOptions({}).flexReload();
							/* 
							if (top.ConfirmMsg(CStr,id,200,80)){
									alert('ok');
								}else{
									alert('no');
								}//訊息Alert
							*/
								//$('#flex1').flexOptions({}).flexReload();
							/*
							if(confirm('確定要刪除此 ' + $('.trSelected',grid).length + ' 筆記錄嗎？'))
							{
							 var  id ="";
						     for(var i=0;i<$('.trSelected',grid).length;i++){
						     	if(i==$('.trSelected',grid).length-1){
						     		id += "id="+$('.trSelected',grid).find("td:first").eq(i).text();
						     	} else {
						     		id += "id="+$('.trSelected',grid).find("td:first").eq(i).text()+"&";
						        }
						      }
							  alert(id);
						      //window.location.href="../ReleaseInfoServlet?hidden="+${"hidden"}.value+"&"+id;
					      }
						  */
					   }
					}
					
				else if (com=='新增')
					{
						//${"hidden"}.value="add";
						//window.location.href="infoAdd.jsp?hidden="+${"hidden"}.value;
						//self.location.href="SUserInfo.php?MODETYPE=1";
					}
					
				else if (com=='修改')
				{
					//${"hidden"}.value="modify";
					if($(".trSelected").length==1){			
					//var thisv = $('.trSelected',grid).find("td").eq(2).text();
					//alert(thisv);
var url2go ="MainAP/EditPD.php?MODETYPE=2&IFS=EditPD&ID=" + $('.trSelected',grid).find("td").eq(0).text();
top.AddIFInfo('編輯<? echo $TARGETID;?>資料','EditPD','820','450',url2go,'1');
					
					
					}else if($(".trSelected").length>1){
						//alert("請一次選擇一位管理者進行編輯");
						top.AlertMsg('請一次選擇一筆資料進行編輯。',250,30);//訊息Alert
					}else if($(".trSelected").length==0){
						//alert("請先選擇要修改的管理者");
						top.AlertMsg('請先選擇要修改的資料。',220,30);//訊息Alert
					}
				}
			}  


function reloadTable1(){
	alert('reload1');
	//$('#flex1').flexOptions({}).flexReload();
	}
</script>
<? //echo $STR;?>
</body>
</html>