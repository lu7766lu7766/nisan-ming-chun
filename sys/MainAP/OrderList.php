<?
session_start();
//ini_set("session.cookie_httponly", 1);
require_once('../config/KEY_SESSION.php'); 

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>訂單列表</title>
		<link type="text/css" href="nCSS/css/start/jquery-ui-1.8.18.custom.css" rel="stylesheet" />
		<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>

<script type="text/javascript" src="../Scripts/thickbox.js"></script>
<link rel="stylesheet" type="text/css" href="../Scripts/thickbox.css" media="screen" /> 
        
        <script src="js/jquery.ui.datepicker.js"></script> 
		<script src="js/jquery.ui.datepicker-zh-TW.js"></script> 
        
<script type="text/javascript">
/*  
  $(document).ready(function() {
    $(".listview tr").mouseover(function() {
        $(this).addClass("over");
      }).mouseout(function() {
        $(this).removeClass("over");
      });
    $(".listview tr:even").addClass("alt");
  });
*/  
  
$(function(){  
  $("#Sdate").datepicker({dateFormat : 'yy/mm/dd'});
  $("#Edate").datepicker({dateFormat : 'yy/mm/dd'});
  GetDBDate();
}); 

function GetDBDate(){
			var Sdate = $("#Sdate").val();
			var Edate = $("#Edate").val();
			var Swords = $("#Swords").val();
			var SOrdSatus = $("#SOrdSatus").val();
			$.ajax({
			url:"ShowDATA.php",
			type:"POST",
			dataType: "text",
			data:{Sdate:Sdate,Edate:Edate,Swords:Swords,SOrdSatus:SOrdSatus},
			success:function(msg){
				//alert(msg);
				$("#ShowDATA").html(msg);
				$("#ShowDATA").fadeIn(500);
				
				    $(".listview tr").mouseover(function() {
						$(this).addClass("over");
					  }).mouseout(function() {
						$(this).removeClass("over");
					  });
					$(".listview tr:even").addClass("alt");
				}
			});	
	
	} 
	

function ShowDetOrder(AMsg,wpx,hpx,Atext){//SHOW出訂單明細
	//alert('hi');
	var showMSGURL = 'Showdetail.php?OrdNO=' + AMsg + '&placeValuesBeforeTB_=savedValues&TB_iframe=true&height=' + hpx + '&width=' + wpx + '&modal=false';
	var caption = Atext;//Atitle;
	var url = showMSGURL;//Ahref;
	tb_show(caption, url);//title,href,rel
	} 	
</script>

<style type="text/css">
		body{
			font:100% "Trebuchet MS", sans-serif;
			margin: 0px;
			margin-left: 10px;
			margin-top: 10px;
			margin-right: 10px;
			margin-bottom: 19px;
		}
	.hideit{
		display:none;
		}
    table {
		margin-left:0px;
        border-collapse: collapse;
        width: 970px;
        
        text-align: center;
    }
    th {
        background: #3e83c9;
        color: #ffffff;
        font-weight: bold;
        padding: 2px 11px;
        border-right: 1px solid #ffffff;
    }
    td {
        padding: 1px 1px;
        border-bottom: 1px solid #95bce2;
        vertical-align: middle;
		text-align:left;
    }
    td * {
		vertical-align: middle;
        padding: 1px 1px;
    }
    tr.alt td {
        background: #ecf6fc;
    }
    tr.over td {
        background: #bcd4ec;
    }
	td div {
		background-color: #bcd4ec;
		height:32px;
		padding-top:12px;
    }
	td div p {
		background-color: #ecf6fc;
		height:22px;
    }
	
	.bluestye {
		font:100% "Trebuchet MS", sans-serif;
		margin-top:0px;
		color:#00F;
	}
	.memostye {
		font:100% "Trebuchet MS", sans-serif;
		color:#666;
	}



	.testg {
		float: left;
		margin:1px;
		width:100px;
		font-size: 12px;
		text-align:center;
		color:#000;
		background-image:url(nCSS/css/start/images/ui-bg_gg.png);
		background-repeat: no-repeat; 
		background-position: 0px 50%;
		padding-top:7px;
		height:24px;

		cursor: pointer;

		}

	.selit {
		
		background-image:url(nCSS/css/start/images/ui-bg_gloss-wave_50_6eac2c_500x100.png);
		background-repeat: no-repeat; 
		background-position: 0px 50%;
		margin:1px;
		text-align:center;
		padding-top:9px;
		width:100px;
		height:28px;
		color:#FFF;
		cursor: pointer;
		/*
		background-color: #39F;
		font-weight: bolder; 
		*/
		}	
	.GBtng {
		width:300px;
		font-size: 16px;
		cursor: pointer;
		margin:1px;
		text-align:center;
		padding-top:9px;
		height:28px;
		background-image:url(nCSS/css/start/images/ui-bg_gloss-wave_45_e14f1c_500x100.png);
		background-repeat: no-repeat; 
		background-position: 0px 50%;
		color:#FFF;		
		}	
		
	.Mbtn {
		
		background-image:url(nCSS/css/start/images/ui-bg_gloss-wave_50_6eac2c_500x100.png);
		background-repeat: no-repeat; 
		background-position: 0px 50%;
		
		text-align:center;
		padding-top:0px;
		width:50px;
		height:22px;
		color:#FFF;
		cursor: pointer;
		/*
		background-color: #39F;
		font-weight: bolder; 
		*/
		}

	.Mustinput {
		border:1px solid red;
		}	
		
	.Readonlyinput {
		border:1px solid blue;
		background-color: #CCC;
		}		
		
	.chpw {
		
		background-image:url(nCSS/css/start/images/ui-bg_gloss-wave_45_e14f1c_500x100.png);
		background-repeat: no-repeat; 
		background-position: 0px 50%;
		margin:1px;
		text-align:center;
		padding-top:1px;
		width:100px;
		height:16px;
		color:#FFF;
		cursor: pointer;
		font-size:10px;
		/*
		background-color: #39F;
		font-weight: bolder; 
		*/
		}	

	  #Edate {background-position:right center; background-repeat:no-repeat;border:1px solid #FFC030;color:#3090C0;font-weight:bold ; font-size:16px; text-align:center}
		
	  #Sdate {background-position:right center; background-repeat:no-repeat;border:1px solid #FFC030;color:#3090C0;font-weight:bold ; font-size:16px; text-align:center}
				
</style>
</head>

<body>

<table width="100%" border="1" class="listview" align="center">
  <tr>
    <td colspan="11">
    <div align="center" style="margin:0px 0px 0px 0px">
    查詢期間：
    <label for="Sdate">起：</label><input type="text" name="Sdate" id="Sdate" size="8" value="<? echo $MYDATE;?>" onchange="GetDBDate()" />
    <label for="Edate">迄：</label><input type="text" name="Edate" id="Edate" size="8" value="<? echo $MYDATE;?>" onchange="GetDBDate()" />
    <label for="Swords">關鍵字查詢：</label><input type="text" name="Swords" id="Swords" onkeyup="GetDBDate()" />
    
    <select name="SOrdSatus" id="SOrdSatus">
        <option value="ALL">全部</option>
    	<option value="1">新訂單</option>
        <option value="2">理貨中</option>
        <option value="3">已出貨</option>
        <option value="11">訂單取消</option>
        <option value="13">退貨中</option>
        <option value="0">錯誤訂單</option>
        <option value="9">訂單完成</option>
    </select>
    
    <input name="" type="button" value="查詢" onclick="GetDBDate()" />
    </div>
    </td>
  </tr>
</table>
<div id="ShowDATA"  style=" width:100%;margin:0px 0px 0px 0px" ></div>




</body>
</html>