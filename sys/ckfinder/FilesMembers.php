<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--
 * CKFinder
 * ========
 * http://ckfinder.com
 * Copyright (C) 2007-2012, CKSource - Frederico Knabben. All rights reserved.
 *
 * The software, this file and its contents are subject to the CKFinder
 * License. Please read the license.txt file before using, installing, copying,
 * modifying or distribute this file or part of its contents. The contents of
 * this file is part of the Source Code of CKFinder.
-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>CKFinder - Sample - Popups</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="robots" content="noindex, nofollow" />
	<link href="sample.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="Scripts/jquery-1.6.min.js"></script>
	<script type="text/javascript" src="ckfinder.js"></script>
	<style type="text/css">
	img {
		padding:10px;
		margin:5px;
		border:1px solid #d5d5d5;
	}
	div.thumb {
		float:left;
	}
	div.caption {
		padding-left:5px;
		font-size:10px;
	}
	body {
	background-color: #FFF;
}
    </style>
	<script type="text/javascript">



function BrowseServer( startupPath, functionData )
{
	// You can use the "CKFinder" class to render CKFinder in a page:
	var finder = new CKFinder();

	// The path for the installation of CKFinder (default = "/ckfinder/").
	finder.basePath = '../';

	//Startup path in a form: "Type:/path/to/directory/"
	finder.startupPath = startupPath;

	// Name of a function which is called when a file is selected in CKFinder.
	finder.selectActionFunction = SetFileField;

	// Additional data to be passed to the selectActionFunction in a second argument.
	// We'll use this feature to pass the Id of a field that will be updated.
	finder.selectActionData = functionData;

	// Name of a function which is called when a thumbnail is selected in CKFinder.
	//finder.selectThumbnailActionFunction = ShowThumbnails;

	// Launch CKFinder
	finder.popup();
}

// This is a sample function which is called when a file is selected in CKFinder.
function SetFileField( fileUrl, data )
{
	document.getElementById( data["selectActionData"] ).value = fileUrl;
}

/*
// This is a sample function which is called when a thumbnail is selected in CKFinder.
function ShowThumbnails( fileUrl, data )
{
	// this = CKFinderAPI
	var sFileName = this.getSelectedFile().name;
	document.getElementById( 'thumbnails' ).innerHTML +=
			'<div class="thumb">' +
				'<img src="' + fileUrl + '" />' +
				'<div class="caption">' +
					'<a href="' + data["fileUrl"] + '" target="_blank">' + sFileName + '</a> (' + data["fileSize"] + 'KB)' +
				'</div>' +
			'</div>';

	document.getElementById( 'preview' ).style.display = "";
	// It is not required to return any value.
	// When false is returned, CKFinder will not close automatically.
	return false;
}
*/

/*
$(function(){
	$("#xImagePath").change(function(){
  		alert('hi');
		$(this).css("background-color","#FFFFCC");
	});
});
*/


function htmlEncode (str){
   var div = document.createElement("div");
   var text = document.createTextNode(str);
   div.appendChild(text);
   return div.innerHTML;
}

function htmlDecode (str){
   var div = document.createElement("div");
   div.innerHTML = str;
   return div.innerHTML;
}

function CHINFO(){
		var thisv  = $("#xImagePath").val();
		var ethisv = htmlEncode(thisv);
		var dthisv = htmlDecode(thisv);
		alert(ethisv + ':' + dthisv);
	}
	</script>
</head>
<body>

	<p>
		<strong>Selected Image URL</strong><br/>
		<input id="xImagePath" name="ImagePath" type="text" size="60" onfocus="CHINFO()" onchange="CHINFO()" onclick="CHINFO()" />
		<input type="button" value="Browse Server" onclick="BrowseServer( 'Images:/', 'xImagePath' );" />
	</p>

</body>
</html>
