<?
function utf8_substr($StrInput,$strStart,$strLen)
{
// 用法 utf8_substr($str,0,40)
$StrInput = mb_substr($StrInput,$strStart,mb_strlen($StrInput));
$iString = urlencode($StrInput);
$lstrResult="";
$istrLen = 0;
$k = 0;
do{
$lstrChar = substr($iString, $k, 1);
if($lstrChar == "%"){
$ThisChr = hexdec(substr($iString, $k+1, 2));
if($ThisChr >= 128){
if($istrLen+3 < $strLen){
$lstrResult .= urldecode(substr($iString, $k, 9));
$k = $k + 9;
$istrLen+=3;
}else{
$k = $k + 9;
$istrLen+=3;
}
}else{
$lstrResult .= urldecode(substr($iString, $k, 3));
$k = $k + 3;
$istrLen+=2;
}
}else{
$lstrResult .= urldecode(substr($iString, $k, 1));
$k = $k + 1;
$istrLen++;
}
}while ($k < strlen($iString) && $istrLen < $strLen); 
return $lstrResult;
}
?>