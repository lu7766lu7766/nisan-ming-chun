<?
if( is_array($_POST) && !get_magic_quotes_gpc())
{
    while( list($k, $v) = each($_POST) )
    {
        $$k = mysql_real_escape_string(trim($v));
    }
    @reset($_POST);
}

//==================================================
function set_var(&$result, $var, $type, $multibyte = false)
{
    settype($var, $type);
    $result = $var;
 
    if ($type == 'string')
    {
        $result = trim(htmlspecialchars(str_replace(array("\r\n", "\r", "\0"), array("\n", "\n", ''), $result), ENT_COMPAT, 'UTF-8'));
        if (!empty($result))
        {
            // Make sure multibyte characters are wellformed
            if ($multibyte)
            {
                if (!preg_match('/^./u', $result))
                {
                    $result = '';
                }
            }
            else
            {
                // no multibyte, allow only ASCII (0-127)
                $result = preg_replace('/[\x80-\xFF]/', '?', $result);
            }
        }

        $result = (STRIP) ? stripslashes($result) : $result;
    }
   
}
?>