<?
session_start();
require_once('../config/SYS_Function.php'); 

$IFS = $_GET["IFS"];
$FIFS = $IFS.'IFS';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Flexigrid</title>
<link rel="stylesheet" type="text/css" href="../Tables/css/flexigrid.css">
<script type="text/javascript" src="../Tables/js/jquery-1.5.2.min.js"></script>
<script type="text/javascript" src="../Tables/js/flexigrid.js"></script>

<style type="text/css">
body {
	margin-left: 5px;
}
</style>
</head>
<body>


<table id="flex1" style="display:none;"></table>
<input id="ActionItem" type="hidden" name="hidden" value="null" />
<script type="text/javascript">
$("#flex1").flexigrid({
        url: 'NewsD_Xml.php',
        dataType: 'xml',
        colModel : [
				{display: '系統編號', 	name : 'MD_NO', width : 80, sortable : true, align: 'center'},
                {display: '標題', 		name : 'MD_Title', width : 180, sortable : true, align: 'center'},
				{display: '說明', 		name : 'MD_ID', width : 280, sortable : true, align: 'center'},
				{display: '排序', 		name : 'MD_Sort', width : 110, sortable : true, align: 'center'},
				{display: '狀態', 		name : 'showyn', width : 30, sortable : true, align: 'left'}
                ], 
			searchitems : [
				{display: '日期標題', name : 'NC_Title', isdefault: true},
				],
			buttons : [
				{name: '新增', bclass: 'add', onpress : button},
				{name: '刪除', bclass: 'delete', onpress : button},
				{name: '修改', bclass: 'modify', onpress : button},				
				{separator: true}
				],				
        sortname: "MD_NO",
		method:'POST', 
        sortorder: "desc",
        usepager: true,
        //title: '使用者列表',
        useRp: true,
        rp: 10,
		rpOptions:[10],
        //showTableToggleBtn: true,
        width: 810,
        //onSubmit: addFormData,
		nomsg: '無資料',
		procmsg: '資料處理中，請稍候 …',
		autoload: true,  
        height: 295
});



			function button(com,grid)
			{
				if (com=='刪除')
					{
						//${"#ActionItem"}.val("delete");
						if($('.trSelected',grid).length==0){
							//alert("请选择要删除的数据");
							top.AlertMsg('請選擇要刪除的名單。',200,30);//訊息Alert
						}else{
							var id ="";
								for(var i=0;i<$('.trSelected',grid).length;i++){
									if(i==$('.trSelected',grid).length-1){
										id += ","+$('.trSelected',grid).find("td:first").eq(i).text();
										//id += ""+$('.trSelected',grid).find("td:first").eq(i).text();
								} else {
										id += ","+$('.trSelected',grid).find("td:first").eq(i).text()+",";
										//id += "&"+$('.trSelected',grid).find("td:first").eq(i).text()+"&";
									}
								}
							
							var CStr = '確定要刪除此 ' + $(".trSelected",grid).length + ' 筆記錄嗎？';
							//top.AlertMsg(CStr,200,30);
							//reloadTable();
							var RUNPAGE = 'model/DelNewsC.php?ID=' + id;
							top.ConfirmMsg(CStr,RUNPAGE,300,80,'<? echo $FIFS;?>');
							$('#flex1').flexOptions({}).flexReload();

					   }
					}
					
				else if (com=='新增')
					{
						//${"hidden"}.value="add";
						//window.location.href="infoAdd.jsp?hidden="+${"hidden"}.value;
						//self.location.href="Edit_NewsC.php?MODETYPE=1&IFS=<? echo $IFS;?>&THISM_ID=&TABLENAME=NewsList&KEYID=MD_NO&CTID=";
						var url2go ="model/Item_model.php?MODETYPE=1&IFS=<? echo $IFS;?>&THISM_ID=&TABLENAME=NewsList&KEYID=MD_NO&CTID=";
						top.AddIFInfo('新增model','AddNewsPageCM','570','500',url2go,'1');
					}
					
				else if (com=='修改')
				{
					//${"hidden"}.value="modify";
					if($(".trSelected").length==1){			
					//var thisv = $('.trSelected',grid).find("td").eq(2).text();
					//alert(thisv);
					var fixID = $('.trSelected',grid).find("td").eq(0).text();
					//self.location.href="SUserInfo.php?MODETYPE=2&BID=" + $('.trSelected',grid).find("td").eq(2).text() + "&ID=" + $('.trSelected',grid).find("td").eq(0).text();
					
					//self.location.href="Edit_NewsC.php?MODETYPE=2&IFS=<? echo $IFS;?>&THISM_ID=" + fixID + "&TABLENAME=NewsList&KEYID=MD_NO&CTID=";
					var url2go ="model/Item_model.php?MODETYPE=2&IFS=<? echo $IFS;?>&THISM_ID=" + fixID + "&TABLENAME=NewsList&KEYID=MD_NO&CTID=&MD_NO=" + fixID;
					top.AddIFInfo('編輯最新消息','AddNewsPageCM','570','500',url2go,'1');
					
					
					}else if($(".trSelected").length>1){
						//alert("請一次選擇一位管理者進行編輯");
						top.AlertMsg('請一次選擇一筆進行編輯。',250,30);//訊息Alert
					}else if($(".trSelected").length==0){
						//alert("請先選擇要修改的管理者");
						top.AlertMsg('請先選擇要修改的資料。',220,30);//訊息Alert
					}
				}
			}  


function reloadTable1(){
	alert('reload1');
	//$('#flex1').flexOptions({}).flexReload();
	}
</script>

</body>
</html>