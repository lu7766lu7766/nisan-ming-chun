<?
session_start();
include('../config/KEY_SESSION.php'); 
include('../config/SYS_Function.php'); 
include("image_functions.php"); 	

$D_NO		= $_POST["PDNO"]; 
$ColorText	= $_POST["btn_ID"];

$W_limit	= '300';
$H_limit	= '470';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>無標題文件</title>
</head>
<link type="text/css" href="nCSS/css/start/jquery-ui-1.8.18.custom.css" rel="stylesheet" />
<link type="text/css" href="../css/animate.css" rel="stylesheet" />

	<script type="text/javascript" src="../Scripts/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="../Scripts/jquery-ui-1.8.18.custom.min.js"></script>

	<script type="text/javascript" src="../Scripts/jquery-pack.js"></script>
	<script type="text/javascript" src="../Scripts/jquery.imgareaselect.min.js"></script>
	<script type="text/javascript" src="../Scripts/jquery.ocupload-packed.js"></script>
    

	
	
<script type="text/javascript">
function HideIFSDivS(IFS){//子訊息承接
		parent.HideIFSDiv(IFS);//訊息Alert	
	}	

function AlertMsgS(errmsg,AMSW,AMSH){//子訊息承接
		parent.ShowHBMSG(errmsg,'1','1');
		parent.AlertMsg(errmsg,AMSW,AMSH);//訊息Alert	
	}	

function AlertMsgSGB(errmsg,AMSW,AMSH,GBURL,GBURLSTR,NowFrameId){//子訊息承接
		parent.ShowHBMSG(errmsg,'1','1');
		parent.AlertMsgGB(errmsg,AMSW,AMSH,GBURL,GBURLSTR,NowFrameId);//訊息Alert	
	}

//show and hide the loading message
function loadingmessage(msg, show_hide ,thisv){

		var loader = '#loader' + thisv;
		var progress = '#progress' + thisv;
		var uploaded_image = '#uploaded_image' + thisv;
		var upload_status = '#upload_status' + thisv;

	if(show_hide=="show"){
		$(loader).show();
		$(progress).show().text(msg);
		$(uploaded_image).html('');
	}else if(show_hide=="hide"){
		$(loader).hide();
		$(progress).text('').hide();
	}else{
		$(loader).hide();
		$(progress).text('').hide();
		$(uploaded_image).html('');
	}
}

//delete the image when the delete link is clicked.
function deleteimage(large_image, thumbnail_image){
	loadingmessage('Please wait, deleting images...', 'show');
	$.ajax({
		type: 'POST',
		url: '<?=$image_handling_file?>',
		data: 'a=delete&large_image='+large_image+'&thumbnail_image='+thumbnail_image,
		cache: false,
		success: function(response){
			loadingmessage('', 'hide');
			response = unescape(response);
			var response = response.split("|");
			var responseType = response[0];
			var responseMsg = response[1];
			if(responseType=="success"){
				$(upload_status).show().html('<h1>Success</h1><p>'+responseMsg+'</p>');
				$(uploaded_image).html('');
			}else{
				$(upload_status).show().html('<h1>Unexpected Error</h1><p>Please try again</p>'+response);
			}
		}
	});
}


//=======================
function test(thisv){
//$(document).ready(function () {
//alert(thisv);
		var thisv = thisv;
		
		var loader = '#loader' + thisv;
		var progress = '#progress' + thisv;
		var uploaded_image = '#uploaded_image' + thisv;
		var upload_status = '#upload_status' + thisv;
		var upload_link = '#upload_link' + thisv;
		
		var MD_Img = '#MD_IMG' + thisv;

		
		$(loader).hide();
		$(progress).hide();
		
		
		var myUpload = $(upload_link).upload({
		   name: 'image',
		   action: '<?=$image_handling_file?>',
		   enctype: 'multipart/form-data',
		   params: {upload:'Upload'},
		   autoSubmit: true,
		   onSubmit: function() {
		   		$(upload_status).html('').hide();
				loadingmessage('Please wait, uploading file...', 'show' , thisv);
				//alert(thisv);
		   },
		   onComplete: function(response) {
		   		loadingmessage('', 'hide');
				response = unescape(response);
				var response = response.split("|");
				var responseType = response[0];
				var responseMsg = response[1];
				if(responseType=="success"){
					var current_width = response[2];
					var current_height = response[3];
					//display message that the file has been uploaded
					$(upload_status).show().html('<h1>Success</h1><p>The image has been uploaded</p>');
					//put the image in the appropriate div
					
					
					
					var Path_Url = '';//'/greatnews/admin/';
					Path_Url = Path_Url + responseMsg;
					
					$(uploaded_image).html('');
					$(uploaded_image).hide();
					$(uploaded_image).html('<img src="'+Path_Url+'" id="nowpic' + thisv + '" style="margin-right: 10px;" id="thumbnail" alt="Create Thumbnail"  width="<? echo $W_limit;?>" height="<? echo $H_limit;?>" />');
					$(uploaded_image).show();
					
					var nowpic;
					nowpic = '#nowpic' + thisv;
					$(nowpic).attr("src",Path_Url);
					//alert(nowpic + ':' + Path_Url);
					
					$(loader).hide();
					$(upload_status).hide();
					$(progress).hide();
					
					//var Path_Url = '/greatnews/admin/';
					//Path_Url = Path_Url + responseMsg;
					$(MD_Img).val(Path_Url);
					$(nowpic).attr("src",Path_Url);
					//alert(responseMsg);

				}else if(responseType=="error"){
					$(upload_status).show().html('<h1>Error</h1><p>'+responseMsg+'</p>');
					$(uploaded_image).html('');
					$('#thumbnail_form').hide();
				}else{
					$(upload_status).show().html('<h1>Unexpected Error</h1><p>Please try again</p>'+response);
					$(uploaded_image).html('');
					$('#thumbnail_form').hide();
				}
		   }
		});
		

}	
//=======================	
</script>

<style>
	.GBtng {
		width:100px;
		font-size: 16px;
		cursor: pointer;
		margin:1px;
		text-align:center;
		padding-top:9px;
		height:28px;
		background-image:url(nCSS/css/start/images/ui-bg_gloss-wave_45_e14f1c_500x100.png);
		background-repeat: no-repeat; 
		background-position: 0px 50%;
		color:#FFF;		
		}	
	
.GBtng1 {
		width:100px;
		font-size: 16px;
		cursor: pointer;
		margin:1px;
		text-align:center;
		
		height:12px;
		background-image:url(nCSS/css/start/images/ui-bg_gloss-wave_45_e14f1c_500x100.png);
		background-repeat: no-repeat; 
		background-position: 0px 50%;
		color:#FFF;		
		}

.GBtng2 {
		width:100px;
		font-size: 16px;
		cursor: pointer;
		margin:1px;
		text-align:center;
		
		height:12px;
		background-image:url(nCSS/css/start/images/ui-bg_gloss-wave_50_6eac2c_500x100.png);
		background-repeat: no-repeat; 
		background-position: 0px 50%;
		color:#FFF;		
		}
		
	.selita {
		float:left;
		
		background-image:url(nCSS/css/start/images/ui-bg_gloss-wave_50_6eac2c_500x100.png);
		background-repeat: no-repeat; 
		background-position: 0px 50%;
		margin:1px;
		text-align:center;
		padding-top:1px;
		width:100px;
		height:20px;
		color:#FFF;
		cursor: pointer;
		/*
		background-color: #39F;
		font-weight: bolder; 
		*/
		}

		.hideit{
			display:none;
		}
</style>
<?


if ($MD_NO  <> ''){
			//處理單筆===================================================
			$Connstr 		= "rowprod";
			$TABLE			= "PD_Model";
			$WhereStr		= "WHERE MD_NO = '".$MD_NO."'  ";
			$AfterOrderBy	= "ORDER BY MD_NO ASC LIMIT 1";
			echo ConnMyDB($Connstr ,$TABLE ,$WhereStr ,$AfterOrderBy);
			//$totalRows_rowprod;//資料筆數
			//===========================================================
} 			
			if ($totalRows_rowprod >= '1'){
				$MODETYPE = '2';

			}else{
				$MODETYPE = '1';
				$MD_Sort = $MY_Y.$MY_m.$MY_d.$MY_H.$MY_i;
			}
		
?>

<body>
<form name="pv" action="../config/PHPDB_General.php" method="post" target="vWORK" >
<input type="hidden" name="TABLENAME" id="TABLENAME" value="PD_Model"  />
<input type="hidden" name="FINDWDFD" id="FINDWDFD" value="MD_NO "  /> 
<input type="hidden" name="FINDWD" id="FINDWD" value="<? echo $MD_NO ;?>"  />


<input type="hidden" name="MODETYPE" id="MODETYPE" value="<? echo $MODETYPE;?>"  />



 

上架：
<?
$showyn_STR = 'showyn'.$showyn;
$$showyn_STR = ' selected="selected"';
?>
      <select class="Mustinput" name="showyn" id="showyn">
        <option value="1" <? echo $showyn1;?>>上架</option>
        <option value="0" <? echo $showyn0;?>>下架</option>
      </select>
  
<BR>
<font color="red" >刪除此筆資料：</font><input type="checkbox" id="DELIT" name="DELIT" value="1" />
<BR>
	<hr>標題：
	<input  type="text" id="MD_Title" name="MD_Title" value="<? echo $MD_Title;?>" size="32"/>
    <br />說明：
	<input  type="text" id="MD_ID" name="MD_ID" value="<? echo $MD_ID;?>" size="52"/>

    <br />排序：
	<input  type="text" id="MD_Sort" name="MD_Sort" value="<? echo $MD_Sort;?>" size="15"/>
    <br /><hr>

<br />	
產品圖片：
<table width="90%" border="1" align="center" width="<? echo $W_limit + 50;?>" height="<? echo $H_limit + 20;?>">
  <tr align="center">	
<?
$PIMGS01_Count = 1;

for ($ii = 1; $ii <= $PIMGS01_Count; $ii++){
	$IDtag = $ii;
	$MDimgID = 'MD_IMGt'.$IDtag;
	
	//echo $$MDimgID.'<br>';
	if ($$MDimgID == ''){
		$$MDimgID = "";
		}
	
	$ii_STR = '圖'.$ii;

	if ($ii == '5'){
		echo '</tr><tr align="center">';
	}
?>
    <td valign="top">
<div style="float:left; color:#666"><!--橫幅編輯--></div>
<font color="#990000" size="2">&nbsp;&nbsp;&nbsp;  <br><br>
 <? echo $ii_STR;?><br><br>(<? echo $W_limit;?>px <? echo $H_limit;?>px  大小：1MB )</font>
<br />					
<div id="uploaded_imaget<? echo $IDtag;?>" style=" width:<? echo $W_limit + 50;?>px;display:<? echo $img_display;?>"><img src="<? echo $$MDimgID;?>" width="150" ></div>
<div id="MDADDt<? echo $IDtag;?>" style="background-color: #FFFFCC;padding:0px 0px 10px 0px;margin:0px 0px 0px 0px;display:block ">
    <div id="upload_statust<? echo $IDtag;?>" style="font-size:12px;  margin:1px; padding:1px; display:none; border:1px #999 dotted; background:#eee;"></div>
    <a id="upload_linkt<? echo $IDtag;?>" style="background:#39f; font-size: 12px; color: white;" onclick="test('t<? echo $IDtag;?>')">『雙擊 置換現在圖檔』</a>
	<span id="loadert<? echo $IDtag;?>" style="display:none;"><img src="loader.gif" alt="Loading..."/></span> 
	<span id="progresst<? echo $IDtag;?>"></span>
	<br />
</div>	    
    </td>
<?	

	}
?>  
  </tr>
</table>


<?
//include("GET_IMG.php");
for ($ii = 1; $ii <= $PIMGS01_Count; $ii++){
	$IDtag = $ii;
	$MDimgID = 'MD_IMGt'.$IDtag;	
	$MDstrID = 'MDstrIDt'.$IDtag;
	$MDurlID = 'MDurlIDt'.$IDtag;
?>
<BR />
圖片<? echo $ii;?>路徑：<input name="MD_IMGt<? echo $ii?>" type="text" id="MD_IMGt<? echo $ii?>" value="<? echo $$MDimgID;?>" size="75">
<!--說明文字：--><input name="MDstrIDt<? echo $ii?>" type="hidden" id="MDstrIDt<? echo $ii?>" size="10" maxlength="50" value="<? echo $$MDstrID;?>">
<!--連結：--><input name="MDurlIDt<? echo $ii?>" type="hidden" id="MDurlIDt<? echo $ii?>" size="50" maxlength="150" value="<? echo $$MDurlID;?>">
<?
}
?>	
	 
</div>
</div>

<div style="width:100%" align="center">
<br><br><br>
<input type="Submit" name="Submit" value="確認" style="font-size:24px" >
<br><br><br>
</div>


</form>
<iframe name="vWORK" src="../Nothing.php" width="0" height="0" scrolling="no" frameborder="0" marginheight="0" marginwidth="0"></iframe>
</body>
</html>