﻿/*
Copyright (c) 2003-2010, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckfinder.com/license
*/

CKFinder.customConfig = function( config )
{
	// Define changes to default configuration here. For example:
	// config.skin = 'v1';
	config.language = 'zh-tw';
	
	//隱藏help按鈕
	config.disableHelpButton = true,
	//設定中文語系
	config.defaultLanguage = 'zh-tw';
	//全視窗
	config.width = '100%';
	config.height = '100%';
	
	
	
	
	
};
