﻿/*
 * CKFinder
 * ========
 * http://ckfinder.com
 * Copyright (C) 2007-2010, CKSource - Frederico Knabben. All rights reserved.
 *
 * The software, this file and its contents are subject to the CKFinder
 * License. Please read the license.txt file before using, installing, copying,
 * modifying or distribute this file or part of its contents. The contents of
 * this file is part of the Source Code of CKFinder.
 *
 */

/**
 * @fileOverview Defines the {@link CKFinder.lang} object, for the Chinese (Taiwan)
 *		language. This is the base file for all translations.
*/

/**
 * Constains the dictionary of language entries.
 * @namespace
 */
CKFinder.lang['zh-tw'] =
{
	appTitle : 'CKFinder', // MISSING

	// Common messages and labels.
	common :
	{
		// Put the voice-only part of the label in the span.
		unavailable		: '%1<span class="cke_accessibility">, 無法使用</span>', // MISSING
		confirmCancel	: '部份內容尚未存檔,您確定要關閉對話框?', // MISSING
		ok				: '確定', // MISSING
		cancel			: '取消', // MISSING
		confirmationTitle	: '確認', // MISSING
		messageTitle	: '提示', // MISSING
		inputTitle		: '詢問', // MISSING
		undo			: '恢復', // MISSING
		redo			: '重做', // MISSING
		skip			: '略過', // MISSING
		skipAll			: '全部略過', // MISSING
		makeDecision	: '您想採取何種措施?', // MISSING
		rememberDecision: '下次不再詢問'  // MISSING
	},

	dir : '由左向右', // MISSING
	HelpLang : 'zh-tw',
	LangCode : 'zh-tw',

	// Date Format
	//		d    : Day
	//		dd   : Day (padding zero)
	//		m    : Month
	//		mm   : Month (padding zero)
	//		yy   : Year (two digits)
	//		yyyy : Year (four digits)
	//		h    : Hour (12 hour clock)
	//		hh   : Hour (12 hour clock, padding zero)
	//		H    : Hour (24 hour clock)
	//		HH   : Hour (24 hour clock, padding zero)
	//		M    : Minute
	//		MM   : Minute (padding zero)
	//		a    : Firt char of AM/PM
	//		aa   : AM/PM
	DateTime : 'yyyy年mm月dd日 h:MM aa',
	DateAmPm : ['上午', '下午'],

	// Folders
	FoldersTitle	: '目錄',
	FolderLoading	: '載入中...',
	FolderNew		: '請輸入新目錄名稱: ',
	FolderRename	: '請輸入新目錄名稱: ',
	FolderDelete	: '確定刪除 "%1" 這個目錄嗎?',
	FolderRenaming	: ' (修改目錄...)',
	FolderDeleting	: ' (刪除目錄...)',

	// Files
	FileRename		: '請輸入新檔案名稱: ',
	FileRenameExt	: '確定變更這個檔案的副檔名嗎? 變更後 , 此檔案可能會無法使用 !',
	FileRenaming	: '修改檔案名稱...',
	FileDelete		: '確定要刪除這個檔案 "%1"?',
	FilesLoading	: '讀取中...', // MISSING
	FilesEmpty		: '尚無任何檔案資料', // MISSING
	FilesMoved		: '檔案 %1 已移動至 %2:%3', // MISSING
	FilesCopied		: '檔案 %1 已複製至 %2:%3', // MISSING

	// Basket
	BasketFolder		: '臨時文件夾',
	BasketClear			: '清空臨時文件夾',
	BasketRemove		: '從臨時文件夾移除',
	BasketOpenFolder	: '打開臨時文件夾',
	BasketTruncateConfirm : '您確定要清空臨時文件夾?',
	BasketRemoveConfirm : '您確定要從臨時文件夾中移除檔案"%1"?',
	BasketEmpty			: '臨時文件夾為空,您可拖曳文件至其中.',
	BasketCopyFilesHere : '從臨時文件夾複製至此',
	BasketMoveFilesHere : '從臨時文件夾移動至此',
	
	BasketPasteErrorOther	: '檔案 %s 錯誤: %e',
	BasketPasteMoveSuccess	: '已移動以下檔案: %s',
	BasketPasteCopySuccess	: '已複製以下檔案: %s',


	// Toolbar Buttons (some used elsewhere)
	Upload		: '上傳檔案',
	UploadTip	: '上傳一個新檔案',
	Refresh		: '重新整理',
	Settings	: '偏好設定',
	Help		: '說明',
	HelpTip		: '說明',

	// Context Menus
	Select			: '選擇',
	SelectThumbnail : '選擇縮圖', // MISSING
	View			: '瀏覽',
	Download		: '下載',

	NewSubFolder	: '建立新子目錄',
	Rename			: '重新命名',
	Delete			: '刪除',

	CopyDragDrop	: '將檔案複製到這裡', // MISSING
	MoveDragDrop	: '將檔案移動到這裡', // MISSING

	// Dialogs
	RenameDlgTitle		: '重新命名', // MISSING
	NewNameDlgTitle		: '新檔案名', // MISSING
	FileExistsDlgTitle	: '檔案已存在', // MISSING
	SysErrorDlgTitle : '系統錯誤', // MISSING

	FileOverwrite	: '自動覆蓋相同名稱', // MISSING
	FileAutorename	: '自動重新新命名相同名稱', // MISSING


	// Generic
	OkBtn		: '確定',
	CancelBtn	: '取消',
	CloseBtn	: '關閉',

	// Upload Panel
	UploadTitle			: '上傳新檔案',
	UploadSelectLbl		: '請選擇要上傳的檔案',
	UploadProgressLbl	: '(檔案上傳中 , 請稍候...)',
	UploadBtn			: '將檔案上傳到伺服器',
	UploadBtnCancel		: '取消', // MISSING

	UploadNoFileMsg		: '請從你的電腦選擇一個檔案',
	UploadNoFolder		: '請先選擇一個檔案', // MISSING
	UploadNoPerms		: '無檔案上傳權限.', // MISSING
	UploadUnknError		: '上傳檔案錯誤.', // MISSING
	UploadExtIncorrect	: '此副檔名在系統中無法使用.', // MISSING

	// Settings Panel
	SetTitle		: '設定',
	SetView			: '瀏覽方式:',
	SetViewThumb	: '縮圖預覽',
	SetViewList		: '清單列表',
	SetDisplay		: '顯示欄位:',
	SetDisplayName	: '檔案名稱',
	SetDisplayDate	: '檔案日期',
	SetDisplaySize	: '檔案大小',
	SetSort			: '排序方式:',
	SetSortName		: '依 檔案名稱',
	SetSortDate		: '依 檔案日期',
	SetSortSize		: '依 檔案大小',

	// Status Bar
	FilesCountEmpty : '<此目錄沒有任何檔案>',
	FilesCountOne	: '1 個檔案',
	FilesCountMany	: '%1 個檔案',

	// Size and Speed
	Kb				: '%1 kB',
	KbPerSecond		: '%1 kB/s',

	// Connector Error Messages.
	ErrorUnknown	: '無法連接到伺服器 ! (錯誤代碼 %1)',
	Errors :
	{
	 10 : '不合法的指令.',
	 11 : '連接過程中 , 未指定資源形態 !',
	 12 : '連接過程中出現不合法的資源形態 !',
	102 : '不合法的檔案或目錄名稱 !',
	103 : '無法連接：可能是使用者權限設定錯誤 或您沒有權限!',
	104 : '無法連接：可能是伺服器檔案權限設定錯誤 或您沒有權限!',
	105 : '無法上傳：不合法的副檔名 !',
	109 : '不合法的請求 !',
	110 : '不明錯誤 !',
	115 : '檔案或目錄名稱重複 !',
	116 : '找不到目錄 ! 請先重新整理 , 然後再試一次 !',
	117 : '找不到檔案 ! 請先重新整理 , 然後再試一次 !',
	118 : '目標位置與現在位置相同.', // MISSING
	201 : '伺服器上已有相同的檔案名稱 ! 您上傳的檔案名稱將會自動更改為 "%1"',
	202 : '不合法的檔案 !',
	203 : '不合法的檔案 ! 檔案大小超過預設值 !',
	204 : '您上傳的檔案已經損毀 !',
	205 : '伺服器上沒有預設的暫存目錄 !',
	206 : '檔案上傳程序因為安全因素已被系統自動取消 ! 可能是上傳的檔案內容包含 HTML 碼 !',
	207 : '您上傳的檔案名稱將會自動更改為 "%1"',
	300 : '移動檔案失敗.', // MISSING
	301 : '複製檔案失敗.', // MISSING
	500 : '因為安全因素 , 檔案瀏覽器已被停用 ! 請聯絡您的系統管理者並檢查 CKFinder 的設定檔 config.php !',
	501 : '縮圖預覽功能已被停用 !'
	},

	// Other Error Messages.
	ErrorMsg :
	{
		FileEmpty		: '檔案名稱不能空白 !',
		FileExists		: '檔案 %s 已存在', // MISSING
		FolderEmpty		: '目錄名稱不能空白 !',

		FileInvChar		: '檔案名稱不能包含以下字元： \n\\ / : * ? " < > |',
		FolderInvChar	: '目錄名稱不能包含以下字元： \n\\ / : * ? " < > |',

		PopupBlockView	: '無法在新視窗開啟檔案 ! 請檢查瀏覽器的設定並且針對這個網站 關閉 <封鎖彈跳視窗> 這個功能 !'
	},

	// Imageresize plugin
	Imageresize :
	{
		dialogTitle		: '更改尺寸 %s', // MISSING
		sizeTooBig		: '無法大於原圖尺寸 (%size).', // MISSING
		resizeSuccess	: '影像尺寸已修改.', // MISSING
		thumbnailNew	: '建立縮圖', // MISSING
		thumbnailSmall	: '小 (%s)', // MISSING
		thumbnailMedium	: '中 (%s)', // MISSING
		thumbnailLarge	: '大 (%s)', // MISSING
		newSize			: '設定新尺寸', // MISSING
		width			: '寬度', // MISSING
		height			: '高度', // MISSING
		invalidHeight	: '高度無效.', // MISSING
		invalidWidth	: '寬度無效.', // MISSING
		invalidName		: '檔案名無效.', // MISSING
		newImage		: '建立新影像', // MISSING
		noExtensionChange : '無法更改檔案副檔名.', // MISSING
		imageSmall		: '原影像尺寸過小',  // MISSING
		contextMenuName	: '更改尺寸' // MISSING
	},

	// Fileeditor plugin
	Fileeditor :
	{
		save			: '存檔', // MISSING
		fileOpenError	: '無法打開檔案.', // MISSING
		fileSaveSuccess	: '存檔成功.', // MISSING
		contextMenuName	: '編輯', // MISSING
		loadingFile		: '檔案讀取中，請稍候...' // MISSING
	}
};