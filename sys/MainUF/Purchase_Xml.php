<?php
session_start();
$page = isset($_POST['page']) ? $_POST['page'] : 1;
$rp = isset($_POST['rp']) ? $_POST['rp'] : 10;
$sortname = isset($_POST['sortname']) ? $_POST['sortname'] : 'P_NO';
$sortorder = isset($_POST['sortorder']) ? $_POST['sortorder'] : 'desc';
$query = isset($_POST['query']) ? $_POST['query'] : false;
$qtype = isset($_POST['qtype']) ? $_POST['qtype'] : false;

//=========================================================================================
function GetMIDInfo($TB,$GETV,$GID,$FNAME) {//資料庫名 對應的值 對應的欄位名 回覆欄位值

        $db['default']['hostname'] = $_SESSION["hostname_CONSQL"];
        $db['default']['username'] = $_SESSION["username_CONSQL"];
        $db['default']['password'] = $_SESSION["password_CONSQL"];
        $db['default']['database'] = $_SESSION["database_CONSQL"];
        $active_group = 'default';

        $connect = mysql_connect($db[$active_group]['hostname'],$db[$active_group]['username'],$db[$active_group]['password']) or die ("Error: could not connect to database");
		mysql_query("SET NAMES 'utf8'");
        $db = mysql_select_db($db[$active_group]['database']);


		$CONNstr_M_Modes = "SELECT * FROM ".$TB." WHERE ".$GID." = '".$GETV."' ORDER BY ".$GID." ASC LIMIT 1";
		$Result_M_Modes = mysql_query($CONNstr_M_Modes,$connect);  
		$row_M_Modes = mysql_fetch_assoc($Result_M_Modes);      
		$sn_M_Modes = mysql_num_rows($Result_M_Modes);
		
		if ($sn_M_Modes >=1 ){
		$WGETV = $row_M_Modes[$FNAME];
		}
		
        return $WGETV;
        mysql_close($connect);
}	

//=========================================================================================



//* -- To use the SQL, remove this block
$usingSQL = true;
function runSQL($rsql) {

        $db['default']['hostname'] = $_SESSION["hostname_CONSQL"];
        $db['default']['username'] = $_SESSION["username_CONSQL"];
        $db['default']['password'] = $_SESSION["password_CONSQL"];
        $db['default']['database'] = $_SESSION["database_CONSQL"];;

        $active_group = 'default';

        $base_url = "http://".$_SERVER['HTTP_HOST'];
        $base_url .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);

        $connect = mysql_connect($db[$active_group]['hostname'],$db[$active_group]['username'],$db[$active_group]['password']) or die ("Error: could not connect to database");
		mysql_query("SET NAMES 'utf8'");
        $db = mysql_select_db($db[$active_group]['database']);

        $result = mysql_query($rsql) or die ('test');
        return $result;
        mysql_close($connect);
}

function countRec($fname,$tname) {
        $sql = "SELECT count($fname) FROM $tname ";
        $result = runSQL($sql);
        while ($row = mysql_fetch_array($result)) {
                return $row[0];
        }
}
$page = $_POST['page'];
$rp = $_POST['rp'];
$sortname = $_POST['sortname'];
$sortorder = $_POST['sortorder'];

if (!$sortname) $sortname = 'P_NO';
if (!$sortorder) $sortorder = 'desc';

$sort = "ORDER BY $sortname $sortorder";

if (!$page) $page = 1;
if (!$rp) $rp = 10;

$start = (($page-1) * $rp);

$limit = "LIMIT $start, $rp";
$where = "";
//if ($query) $where = " WHERE $qtype LIKE '%".mysql_real_escape_string($query)."%' ";
if ($query) $where = " WHERE $qtype LIKE '%".$query."%' ";


$sql = "SELECT * FROM PurchaseBill $where $sort $limit";
$result = runSQL($sql);

$total = countRec('P_NO','PurchaseBill');
$rows = array();
while ($row = mysql_fetch_array($result)) {
        $rows[] = $row;
}
//*/
if(!isset($usingSQL)){
        include dirname(__FILE__).'/countryArray.inc.php';
        if($qtype && $query){
                $query = strtolower(trim($query));
                foreach($rows AS $key => $row){
                        if(strpos(strtolower($row[$qtype]),$query) === false){
                                unset($rows[$key]);
                        }
                }
        }
        //Make PHP handle the sorting
        $sortArray = array();
        foreach($rows AS $key => $row){
                $sortArray[$key] = $row[$sortname];
        }
        $sortMethod = SORT_ASC;
        if($sortorder == 'desc'){
                $sortMethod = SORT_DESC;
        }
        array_multisort($sortArray, $sortMethod, $rows);
        $total = count($rows);
        $rows = array_slice($rows,($page-1)*$rp,$rp);
}


header("Content-type: text/xml");
$xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
$xml .= "<rows>";
$xml .= "<page>$page</page>";
$xml .= "<total>$total</total>";
foreach($rows AS $row){
	
	
	//$BUA[$broadcast_id] = $broadcast_title;
		//$broadcast_id = $row['broadcast_id'];
		//$broadcast_id_Str = $BUA[$broadcast_id];
		GetMIDInfo('FirmINFO',$row['UF_NO'],'UF_NO','UFNAME');//資料庫名 對應的值 對應的欄位名 回覆欄位值

        $xml .= "<row id='".$row['P_NO']."'>";
		$xml .= "<cell><![CDATA[".$row['P_NO']."]]></cell>";
		$xml .= "<cell><![CDATA[".$row['P_Number']."]]></cell>";
        $xml .= "<cell><![CDATA[".GetMIDInfo('FirmINFO',$row['UF_NO'],'UF_NO','UFNAME')."]]></cell>";
        $xml .= "<cell><![CDATA[".GetMIDInfo('admin',$row['Purchaser'],'admin_id','admin_name')."]]></cell>";
        $xml .= "<cell><![CDATA[".$row['Total']."]]></cell>";
		$xml .= "<cell><![CDATA[".$row['P_Date']."]]></cell>";
		$xml .= "<cell><![CDATA[".$row['showyn']."]]></cell>";
        $xml .= "</row>";	
}

$xml .= "</rows>";
echo $xml;