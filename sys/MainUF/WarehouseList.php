<?
session_start();
require_once('../config/SYS_Function.php'); 

$TARGETID = '倉庫';

$IFS 		= $_GET["IFS"];
$TABLENAME	= 'Warehouse_Unit';
$KEYID		= 'DU_NO';
$KEYF		= 'DU_ID';
$GBURL		= 'WarehouseList.php';
$ComStr		= "IFS=".$IFS."&TABLENAME=".$TABLENAME."&KEYID=".$KEYID."&KEYF=".$KEYF."&GBURL=".$GBURL;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Flexigrid</title>
<link rel="stylesheet" type="text/css" href="../Tables/css/flexigrid.css">
<script type="text/javascript" src="../Tables/js/jquery-1.5.2.min.js"></script>
<script type="text/javascript" src="../Tables/js/flexigrid.js"></script>

<style type="text/css">
body {
	margin-left: 5px;
}
</style>
</head>
<body>

<table id="flex1" style="display:none;"></table>
<input id="ActionItem" type="hidden" name="hidden" value="null" />
<script type="text/javascript">
$("#flex1").flexigrid({
        url: 'Warehouse_Xml.php',
        dataType: 'xml',
        colModel : [
{display: '系統編號', 	name : 'DU_NO', width : 40, sortable : true, align: 'center',hide: true},
{display: '<? echo $TARGETID;?>名稱', 	name : 'DU_ID', width : 320, sortable : true, align: 'center'},
{display: '啟用', 		name : 'showyn', width : 50, sortable : true, align: 'left'}
                ],
			searchitems : [
				{display: '名稱', name : 'DU_ID', isdefault: true}
				],
			buttons : [
				{name: '新增<? echo $TARGETID;?>資料', bclass: 'add', onpress : button},
				{name: '刪除', bclass: 'delete', onpress : button},	
				{name: '編輯<? echo $TARGETID;?>資料', bclass: 'modify', onpress : button},	
				{separator: true}
				],				
        sortname: "DU_NO",
		method:'POST', 
        sortorder: "asc",
        usepager: true,
        //title: '使用者列表',
        useRp: true,
        rp: 10,
		rpOptions:[10],
        //showTableToggleBtn: true,
        width: 400,
        //onSubmit: addFormData,
		nomsg: '無資料',
		procmsg: '資料處理中，請稍候 …',
		autoload: true,  
        height: 410
});



			function button(com,grid)
			{
				if (com=='刪除')
					{
						//${"#ActionItem"}.val("delete");
						if($('.trSelected',grid).length==0){
							//alert("请选择要删除的数据");
							top.AlertMsg('請選擇要刪除的資料。',200,30);//訊息Alert
						}else if($(".trSelected").length>1){
						//alert("請一次選擇一位管理者進行編輯");
						top.AlertMsg('請一次選擇一筆資料刪除。',250,30);//訊息Alert								
							
						}else{
							var id ="";
								for(var i=0;i<$('.trSelected',grid).length;i++){
									if(i==$('.trSelected',grid).length-1){
										id += ","+$('.trSelected',grid).find("td:first").eq(i).text();
										//id += ""+$('.trSelected',grid).find("td:first").eq(i).text();
								} else {
										id += ","+$('.trSelected',grid).find("td:first").eq(i).text()+",";
										//id += "&"+$('.trSelected',grid).find("td:first").eq(i).text()+"&";
									}
								}
							
							var CStr = '確定要刪除此 ' + $(".trSelected",grid).length + ' 筆記錄嗎？<br>(該資料將永久刪除)';
							//top.AlertMsg(CStr,200,30);
							//reloadTable();
							//var RUNPAGE = 'function/DelUser.php?ID=' + id;
							var RUNPAGE = 'MainUF/DelUnit.php?ID=' + id + '&<? echo $ComStr;?>';
							//alert(RUNPAGE);
							top.ConfirmMsgUnit(CStr,RUNPAGE,300,80,'<? echo $IFS;?>IFS');
							$('#flex1').flexOptions({}).flexReload();
					   }
					}
					
				else if (com=='新增<? echo $TARGETID;?>資料')
					{
/*
$IFS 		= $_GET["IFS"];
$TABLENAME	= $_GET["TABLENAME"];
$KEYID		= 'DU_NO';
$KEYF		= 'DU_ID';
$GBURL		= 'WarehouseList.php';
*/

var url2go ="MainUF/Warehouse_Work.php?MODETYPE=1&<? echo $ComStr;?>";
top.AddIFInfo('新增<? echo $TARGETID;?>資料','Upstreamfirms','410','150',url2go,'0');						
					}



					
				else if (com=='編輯<? echo $TARGETID;?>資料')
				{
					//${"hidden"}.value="modify";
					if($(".trSelected").length==1){			
					//var thisv = $('.trSelected',grid).find("td").eq(2).text();
					//alert(thisv);
					
					//self.location.href="SUserInfo.php?MODETYPE=2&BID=" + $('.trSelected',grid).find("td").eq(2).text() + "&ID=" + $('.trSelected',grid).find("td").eq(0).text();
//MainUF/InsertUF.php?IFS=Upstreamfirms&MODETYPE=1
//var url2go ="MainUF/InsertUF.php?MODETYPE=2&IFS=Upstreamfirms&ID=" + $('.trSelected',grid).find("td").eq(0).text();
//top.AddIFInfo('編輯<? echo $TARGETID;?>資料','Upstreamfirms','910','550',url2go,'1');

var url2go ="MainUF/Warehouse_Work.php?MODETYPE=2&<? echo $ComStr;?>&ID=" + $('.trSelected',grid).find("td").eq(0).text();
top.AddIFInfo('編輯<? echo $TARGETID;?>資料','Upstreamfirms','410','150',url2go,'0');						
					
					
					}else if($(".trSelected").length>1){
						//alert("請一次選擇一位管理者進行編輯");
						top.AlertMsg('請一次選擇一筆資料編輯。',250,30);//訊息Alert
					}else if($(".trSelected").length==0){
						//alert("請先選擇要修改的管理者");
						top.AlertMsg('請先選擇要修改的資料。',220,30);//訊息Alert
					}
				}
				
				
			}  


function reloadTable1(){
	alert('reload1');
	//$('#flex1').flexOptions({}).flexReload();
	}
</script>

</body>
</html>