<?
session_start();
require_once('../config/SYS_Function.php'); 

$IFS 		= $_GET["IFS"];
$TARGETID = '廠商';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Flexigrid</title>
<link rel="stylesheet" type="text/css" href="../Tables/css/flexigrid.css">
<script type="text/javascript" src="../Tables/js/jquery-1.5.2.min.js"></script>
<script type="text/javascript" src="../Tables/js/flexigrid.js"></script>

<style type="text/css">
body {
	margin-left: 5px;
}
</style>
</head>
<body>

<table id="flex1" style="display:none;"></table>
<input id="ActionItem" type="hidden" name="hidden" value="null" />
<script type="text/javascript">
$("#flex1").flexigrid({
        url: 'Firm_Xml.php',
        dataType: 'xml',
        colModel : [
{display: '系統編號', 	name : 'UF_NO', width : 40, sortable : true, align: 'center',hide: true},
{display: '廠商代號', 	name : 'UFID', width : 80, sortable : true, align: 'center'},
{display: '公司名稱', 	name : 'UFNAME', width : 330, sortable : true, align: 'left'},
{display: '聯絡人', 		name : 'UFCNAME', width : 150, sortable : true, align: 'left'},
{display: '統編', 		name : 'UFUNID', width : 80, sortable : true, align: 'left'},
{display: '聯絡電話1', 	name : 'TEL1', width : 90, sortable : true, align: 'left'},
{display: '行動電話', 	name : 'MTEL', width : 90, sortable : true, align: 'left'}
                ],
			searchitems : [
				{display: '帳號', name : 'UID', isdefault: true},
				{display: '姓名', name : 'UNAME'},
				{display: '加入日期', name : 'AddDATE'},
				{display: '生日', name : 'UBDATE'},
				{display: '行動電話', name : 'UMTEL'}
				],
			buttons : [
				{name: '新增廠商資料', bclass: 'add', onpress : button},
				/*{name: '刪除', bclass: 'delete', onpress : button},*/	
				{name: '編輯廠商資料', bclass: 'modify', onpress : button},	
				{name: '新增進貨單', bclass: 'addPurchase', onpress : button},
				{name: '新增發票', bclass: 'addInvoice', onpress : button},				
				{separator: true}
				],				
        sortname: "UF_NO",
		method:'POST', 
        sortorder: "asc",
        usepager: true,
        //title: '使用者列表',
        useRp: true,
        rp: 10,
		rpOptions:[10],
        //showTableToggleBtn: true,
        width: 900,
        //onSubmit: addFormData,
		nomsg: '無資料',
		procmsg: '資料處理中，請稍候 …',
		autoload: true,  
        height: 410
});



			function button(com,grid)
			{
				if (com=='刪除')
					{
						//${"#ActionItem"}.val("delete");
						if($('.trSelected',grid).length==0){
							//alert("请选择要删除的数据");
							top.AlertMsg('請選擇要刪除的名單。',200,30);//訊息Alert
						}else{
							var id ="";
								for(var i=0;i<$('.trSelected',grid).length;i++){
									if(i==$('.trSelected',grid).length-1){
										id += ","+$('.trSelected',grid).find("td:first").eq(i).text();
										//id += ""+$('.trSelected',grid).find("td:first").eq(i).text();
								} else {
										id += ","+$('.trSelected',grid).find("td:first").eq(i).text()+",";
										//id += "&"+$('.trSelected',grid).find("td:first").eq(i).text()+"&";
									}
								}
							
							var CStr = '確定要刪除此 ' + $(".trSelected",grid).length + ' 筆記錄嗎？<br>(該管理者所有檔案將永久刪除)';
							//top.AlertMsg(CStr,200,30);
							//reloadTable();
							var RUNPAGE = 'function/DelUser.php?ID=' + id;
							top.ConfirmMsg(CStr,RUNPAGE,300,80,'UMIFS');
							$('#flex1').flexOptions({}).flexReload();
							/* 
							if (top.ConfirmMsg(CStr,id,200,80)){
									alert('ok');
								}else{
									alert('no');
								}//訊息Alert
							*/
								//$('#flex1').flexOptions({}).flexReload();
							/*
							if(confirm('確定要刪除此 ' + $('.trSelected',grid).length + ' 筆記錄嗎？'))
							{
							 var  id ="";
						     for(var i=0;i<$('.trSelected',grid).length;i++){
						     	if(i==$('.trSelected',grid).length-1){
						     		id += "id="+$('.trSelected',grid).find("td:first").eq(i).text();
						     	} else {
						     		id += "id="+$('.trSelected',grid).find("td:first").eq(i).text()+"&";
						        }
						      }
							  alert(id);
						      //window.location.href="../ReleaseInfoServlet?hidden="+${"hidden"}.value+"&"+id;
					      }
						  */
					   }
					}
					
				else if (com=='新增廠商資料')
					{
						//${"hidden"}.value="add";
						//window.location.href="infoAdd.jsp?hidden="+${"hidden"}.value;
						//self.location.href="SUserInfo.php?MODETYPE=1";
var url2go ="MainUF/InsertUF.php?MODETYPE=1&IFS=Upstreamfirms";
top.AddIFInfo('編輯<? echo $TARGETID;?>資料','Upstreamfirms','910','550',url2go,'1');						
					}


				else if (com=='新增進貨單')
				{
						
					if($(".trSelected").length==1){	
var url2go ="MainUF/AddPurchase.php?MODETYPE=1&IFS=AddPurchase&UF_NO=" + $('.trSelected',grid).find("td").eq(0).text();
//alert($('.trSelected',grid).find("td").eq(0).text());
top.AddIFInfo('新增進貨單','AddPurchase','910','550',url2go,'1');	
					}else if($(".trSelected").length>1){
						//alert("請一次選擇一位管理者進行編輯");
						top.AlertMsg('請一次選擇一筆<? echo $TARGETID;?>進行新增進貨單。',280,30);//訊息Alert
					}else if($(".trSelected").length==0){
						//alert("請先選擇要修改的管理者");
						top.AlertMsg('請先選擇<? echo $TARGETID;?>。',220,30);//訊息Alert
					}
				}
					
					
				else if (com=='新增發票')
				{
						
					if($(".trSelected").length==1){	
var url2go ="MainUF/AddInvoice.php?MODETYPE=1&IFS=AddInvoice&UF_NO=" + $('.trSelected',grid).find("td").eq(0).text();
//alert($('.trSelected',grid).find("td").eq(0).text());
top.AddIFInfo('新增發票','AddInvoice','910','550',url2go,'1');	
					}else if($(".trSelected").length>1){
						//alert("請一次選擇一位管理者進行編輯");
						top.AlertMsg('請一次選擇一筆<? echo $TARGETID;?>進行新增發票。',280,30);//訊息Alert
					}else if($(".trSelected").length==0){
						//alert("請先選擇要修改的管理者");
						top.AlertMsg('請先選擇<? echo $TARGETID;?>。',220,30);//訊息Alert
					}
				}					
					
				else if (com=='編輯廠商資料')
				{
					//${"hidden"}.value="modify";
					if($(".trSelected").length==1){			
					//var thisv = $('.trSelected',grid).find("td").eq(2).text();
					//alert(thisv);
					
					//self.location.href="SUserInfo.php?MODETYPE=2&BID=" + $('.trSelected',grid).find("td").eq(2).text() + "&ID=" + $('.trSelected',grid).find("td").eq(0).text();
//MainUF/InsertUF.php?IFS=Upstreamfirms&MODETYPE=1
var url2go ="MainUF/InsertUF.php?MODETYPE=2&IFS=Upstreamfirms&ID=" + $('.trSelected',grid).find("td").eq(0).text();
top.AddIFInfo('編輯<? echo $TARGETID;?>資料','Upstreamfirms','910','550',url2go,'1');

					
					
					}else if($(".trSelected").length>1){
						//alert("請一次選擇一位管理者進行編輯");
						top.AlertMsg('請一次選擇一位管理者進行編輯。',250,30);//訊息Alert
					}else if($(".trSelected").length==0){
						//alert("請先選擇要修改的管理者");
						top.AlertMsg('請先選擇要修改的管理者。',220,30);//訊息Alert
					}
				}
			}  


function reloadTable1(){
	alert('reload1');
	//$('#flex1').flexOptions({}).flexReload();
	}
</script>

</body>
</html>