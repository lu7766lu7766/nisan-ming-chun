<?
session_start();
require_once('../config/SYS_Function.php'); 

$IFS = $_GET["IFS"];
$FIFS = $IFS.'IFS';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Flexigrid</title>
<link rel="stylesheet" type="text/css" href="../Tables/css/flexigrid.css">
<script type="text/javascript" src="../Tables/js/jquery-1.5.2.min.js"></script>
<script type="text/javascript" src="../Tables/js/flexigrid.js"></script>

<style type="text/css">
body {
	margin-left: 5px;
}
</style>
</head>
<body>
<a href="/sys/News/testshow.php">前台</a>
<table id="flex1" style="display:none;"></table>
<input id="ActionItem" type="hidden" name="hidden" value="null" />
<script type="text/javascript">
$("#flex1").flexigrid({
        url: 'NewsD_Xml.php',
        dataType: 'xml',
        colModel : [
				{display: '系統編號', 	name : 'NC_ID', width : 40, sortable : true, align: 'center',hide: true},
                {display: '日期標題', 		name : 'NC_DATE', width : 120, sortable : true, align: 'center'},
				{display: '消息標題', 		name : 'NC_Title', width : 350, sortable : true, align: 'center'},
				{display: '分類', 		name : 'NC_TYPE', width : 70, sortable : true, align: 'center'},
				{display: '關閉/開啟', 		name : 'showyn', width : 80, sortable : true, align: 'center'}
                ],
			searchitems : [
				{display: '日期標題', name : 'NC_Title', isdefault: true},
				{display: '分類', name : 'NC_TYPE', isdefault: true},
				],
			buttons : [
				{name: '新增', bclass: 'add', onpress : button},
				{name: '刪除', bclass: 'delete', onpress : button},
				{name: '修改', bclass: 'modify', onpress : button},				
				{separator: true}
				],				
        sortname: "NC_ID",
		method:'POST', 
        sortorder: "desc",
        usepager: true,
        //title: '使用者列表',
        useRp: true,
        rp: 10,
		rpOptions:[10,15],
        //showTableToggleBtn: true,
        width: 810,
        //onSubmit: addFormData,
		nomsg: '無資料',
		procmsg: '資料處理中，請稍候 …',
		autoload: true,  
        height: 295
});



			function button(com,grid)
			{
				if (com=='刪除')
					{
						//${"#ActionItem"}.val("delete");
						if($('.trSelected',grid).length==0){
							//alert("请选择要删除的数据");
							top.AlertMsg('請選擇要刪除的名單。',200,30);//訊息Alert
						}else{
							var id ="";
								for(var i=0;i<$('.trSelected',grid).length;i++){
									if(i==$('.trSelected',grid).length-1){
										id += ","+$('.trSelected',grid).find("td:first").eq(i).text();
										//id += ""+$('.trSelected',grid).find("td:first").eq(i).text();
								} else {
										id += ","+$('.trSelected',grid).find("td:first").eq(i).text()+",";
										//id += "&"+$('.trSelected',grid).find("td:first").eq(i).text()+"&";
									}
								}
							
							var CStr = '確定要刪除此 ' + $(".trSelected",grid).length + ' 筆記錄嗎？';
							//top.AlertMsg(CStr,200,30);
							//reloadTable();
							var RUNPAGE = 'News/DelNewsC.php?ID=' + id;
							top.ConfirmMsg(CStr,RUNPAGE,300,80,'<? echo $FIFS;?>');
							$('#flex1').flexOptions({}).flexReload();

					   }
					}
					
				else if (com=='新增')
					{
						//${"hidden"}.value="add";
						//window.location.href="infoAdd.jsp?hidden="+${"hidden"}.value;
						//self.location.href="Edit_NewsC.php?MODETYPE=1&IFS=<? echo $IFS;?>&THISM_ID=&TABLENAME=NewsList&KEYID=NC_ID&CTID=";
						var url2go ="News/Edit_NewsC.php?MODETYPE=1&IFS=<? echo $IFS;?>&THISM_ID=&TABLENAME=NewsList&KEYID=NC_ID&CTID=";
						top.AddIFInfo('新增最新消息','AddNewsPageCM','970','500',url2go,'1');
					}
					
				else if (com=='修改')
				{
					//${"hidden"}.value="modify";
					if($(".trSelected").length==1){			
					//var thisv = $('.trSelected',grid).find("td").eq(2).text();
					//alert(thisv);
					var fixID = $('.trSelected',grid).find("td").eq(0).text();
					//self.location.href="SUserInfo.php?MODETYPE=2&BID=" + $('.trSelected',grid).find("td").eq(2).text() + "&ID=" + $('.trSelected',grid).find("td").eq(0).text();
					
					//self.location.href="Edit_NewsC.php?MODETYPE=2&IFS=<? echo $IFS;?>&THISM_ID=" + fixID + "&TABLENAME=NewsList&KEYID=NC_ID&CTID=";
					var url2go ="News/Edit_NewsC.php?MODETYPE=2&IFS=<? echo $IFS;?>&THISM_ID=" + fixID + "&TABLENAME=NewsList&KEYID=NC_ID&CTID=";
					top.AddIFInfo('編輯最新消息','AddNewsPageCM','970','500',url2go,'1');
					
					
					}else if($(".trSelected").length>1){
						//alert("請一次選擇一位管理者進行編輯");
						top.AlertMsg('請一次選擇一位管理者進行編輯。',250,30);//訊息Alert
					}else if($(".trSelected").length==0){
						//alert("請先選擇要修改的管理者");
						top.AlertMsg('請先選擇要修改的管理者。',220,30);//訊息Alert
					}
				}
			}  


function reloadTable1(){
	alert('reload1');
	//$('#flex1').flexOptions({}).flexReload();
	}
</script>

</body>
</html>