-- phpMyAdmin SQL Dump
-- version 3.5.8
-- http://www.phpmyadmin.net
--
-- 主機: localhost
-- 產生日期: 2013 年 07 月 24 日 04:56
-- 伺服器版本: 5.1.70-cll
-- PHP 版本: 5.3.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 資料庫: `drmzaqck_TONWIN`
--

-- --------------------------------------------------------

--
-- 表的結構 `NewsList`
--

CREATE TABLE IF NOT EXISTS `NewsList` (
  `NC_ID` int(11) NOT NULL AUTO_INCREMENT,
  `NC_TYPE` int(11) NOT NULL DEFAULT '1',
  `NC_DATE` varchar(30) CHARACTER SET utf8 NOT NULL,
  `NC_TID` varchar(80) CHARACTER SET utf8 NOT NULL,
  `NC_Title` varchar(200) CHARACTER SET utf8 NOT NULL,
  `NC_Content` text CHARACTER SET utf8 NOT NULL,
  `showyn` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`NC_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='公告' AUTO_INCREMENT=3 ;

--
-- 轉存資料表中的資料 `NewsList`
--

INSERT INTO `NewsList` (`NC_ID`, `NC_TYPE`, `NC_DATE`, `NC_TID`, `NC_Title`, `NC_Content`, `showyn`) VALUES
(1, 1, '2013/07/10', '', '測試標題', '&nbsp;test', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
