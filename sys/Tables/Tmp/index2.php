<table id="flex1" style="display:none"></table>
			<input id="hidden" type="hidden" name="hidden" value="null" />
			<script>
      		$("#flex1").flexigrid
			(
			{
			url: '../ReleaseInfoServlet?hidden=manage',
			dataType: 'json',
			colModel : [
				{display: '信息编号', name : 'RINO', width : 50, sortable : true, align: 'center',hide: false},
				{display: '信息标题', name : 'RITITLE', width : 250, sortable : true, align: 'center'},
				{display: '信息类别', name : 'IC.ICNAME', width : 100, sortable : true, align: 'center'},
				{display: '信息热点', name : 'RIHOTPOINT', width : 60, sortable : true, align: 'center'},
			<!--{display: '信息内容', name : 'RICONTENT', width : 100, sortable : true, align: 'center'},-->
				{display: '发布日期', name : 'RIDATE', width : 120, sortable : true, align: 'center'},
				{display: '发布作者', name : 'RIAUTHOR', width : 80, sortable : true, align: 'center'}																
				],
			buttons : [
				{name: '添加', bclass: 'add', onpress : button},
				{name: '删除', bclass: 'delete', onpress : button},
				{name: '修改', bclass: 'modify', onpress : button},				
				{separator: true}
				],
			searchitems : [
				{display: '信息编号', name : 'RINO', isdefault: true},
				{display: '信息标题', name : 'RITITLE'},
				{display: '信息类别', name : 'IC.ICNAME'},
				{display: '发布作者', name : 'RIAUTHOR'}
				],
			sortname: "RINO",
			sortorder: "desc",
			usepager: true,
			title: '信息发布管理',
			useRp: true,
			rp: 20,
			showTableToggleBtn: true,
			width: 780,
			height: 300
			}
			);
			
			function button(com,grid)
			{
				if (com=='删除')
					{
						${"hidden"}.value="delete";
						if($('.trSelected',grid).length==0){
							alert("请选择要删除的数据");
						}else{
							if(confirm('是否删除这 ' + $('.trSelected',grid).length + ' 条记录吗?'))
							{
							  var  id ="";
						     for(var i=0;i<$('.trSelected',grid).length;i++){
						     	if(i==$('.trSelected',grid).length-1){
						     		id += "id="+$('.trSelected',grid).find("td:first").eq(i).text();
						     	} else {
						     		id += "id="+$('.trSelected',grid).find("td:first").eq(i).text()+"&";
						        }
						      }
						      window.location.href="../ReleaseInfoServlet?hidden="+${"hidden"}.value+"&"+id;
					      }
					   }
					}
				else if (com=='添加')
					{
						${"hidden"}.value="add";
						window.location.href="infoAdd.jsp?hidden="+${"hidden"}.value;
					}
				else if (com=='修改')
				{
					${"hidden"}.value="modify";
					if($(".trSelected").length==1){
						window.location.href="infoAdd.jsp?hidden="+${"hidden"}.value+"&id="+$('.trSelected',grid).find("td").eq(0).text();
					}else if($(".trSelected").length>1){
						alert("请选择一个修改,不能同时修改多个");
					}else if($(".trSelected").length==0){
						alert("请选择一个您要修改的新闻信息")
					}
				}
			}  
