<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Flexigrid</title>
<link rel="stylesheet" type="text/css" href="css/flexigrid.css">
<script type="text/javascript" src="js/jquery-1.5.2.min.js"></script>
<script type="text/javascript" src="js/flexigrid.js"></script>

</head>
<body>

<table id="flex1" style="display:none"></table>

<script type="text/javascript">
$("#flex1").flexigrid({
        url: 'Date_Xml.php',
        dataType: 'xml',
        colModel : [
                {display: '單位', 		name : 'broadcast_id', width : 40, sortable : true, align: 'center'},
                {display: '管理者帳號', 	name : 'admin_acc', width : 80, sortable : true, align: 'left'},
                {display: '管理者姓名', 	name : 'admin_name', width : 120, sortable : true, align: 'left'},
                {display: '管理者職稱', 	name : 'admin_title', width : 130, sortable : true, align: 'left'},
				{display: '管理者信箱', 	name : 'admin_mail', width : 130, sortable : true, align: 'left'},
				{display: '啟用', 		name : 'admin_verify', width : 30, sortable : true, align: 'left'},
                {display: '管理者電話', 	name : 'admin_phone', width : 80, sortable : true, align: 'center'}
                ],
			searchitems : [
				{display: '單位', name : 'broadcast_id', isdefault: true},
				{display: '管理者帳號', name : 'admin_acc'},
				{display: '管理者姓名', name : 'admin_name'},
				{display: '管理者電話', name : 'admin_phone'}
				],
        sortname: "broadcast_id",
		method:'POST', 
        sortorder: "asc",
        usepager: true,
        title: '使用者列表',
        useRp: true,
        rp: 15,
        //showTableToggleBtn: true,
        width: 700,
        //onSubmit: addFormData,
		nomsg: '無資料',
		procmsg: '資料處理中，請稍候 …',
		autoload: true,  
        height: 200
});


/*
$('#EmployeeGrid').flexigrid({    
    //表格寬度(注意在IE不能使用100%之類字串)    
     width:$(window).width()-2,    
    //表格高度(注意在IE不能使用100%之類字串)    
     height:480,    
    //資料列雙色交差    
     striped:true,    
    //欄位雙色交差    
     novstripe:false,    
    //最小寬度    
     minwidth:400,    
    //最小高度    
     minheight:200,    
    //是否可調整視窗大小    
     resizable:true,    
    //遠端伺服的網址    
     url:'server/employee.php',    
    //資料送出模式    
     method:'POST',    
    //回傳的資料類型    
     dataType:'xml',    
    //連結資料失敗時的訊息    
     errormsg:'連線資料庫失敗',    
    //啟用分頁器    
     usepager:true,    
    //不重疊    
     nowrap:true,    
    //預設的頁數    
     page:1,//当前页，但是该参数在初始化有效，在后期需要跳页时，需通过修改 newp 这个参数 


    //總頁數    
     total:1,//totalpages    
    //使用分頁大小選擇器    
     useRp:true,    
    //預設的分頁大小(筆數)    
     rp:15,//resultsperpage    
    //可選用的分頁大小    
     rpOptions:[10,15,20,25,40],    
    //標題    
     title:false,    
    //分頁器的顯示資訊    
     pagestat:'檢視{from}到{to}，全部共有{total}筆資料',    
    //讀取時的訊息    
     procmsg:'資料讀取中，請稍後…',    
    //搜尋時送出附加自訂的query    
     query:'',    
    //搜尋自訂附加的欄位，請搜尋"//addsearchbutton"參考原始碼那段    
     qtype:'',    
    //空資料時的訊息    
     nomsg:'找不到符合絛件的資料',    
    //隱藏欄位數不得少於?    
     minColToggle:1,    
    //顯示或關閉隱藏欄位的開啟器    
     showToggleBtn:true,    
    //預設排序的欄位    
     sortname:'basic_name',    
    //預設排序的方式    
     sortorder:'asc',    
    //送出時隱藏    
     hideOnSubmit:false,    
    //限定單選    
     singleSelect:true,    
    //不得調整視窗寬度    
     nohresize:true   
    //自動讀取資料    
     autoload:true,    
    //區塊透明    
     blockOpacity:0.5,    
    //顯示隱藏欄位時呼叫的自訂函式    
     onToggleCol:false,    
    //改變排序方式時呼叫的自訂函式    
     onChangeSort:false,    
    //執行成功後呼叫的自訂函式    
     onSuccess:false,    
    //資料送出時呼叫的自訂函式    
     onSubmit:false,    
    //錯誤時呼叫的自訂函式    
     onError:false,    
    //當分頁大小選擇器被選擇時呼叫自訂函式    
     onRpChange:false,    
    //是否顯示右上角縮小視窗的按鈕    
     showTableToggleBtn:true,    
    //定義欄位資訊(以下為範例)    
     colModel:[    
        //第一個欄位    
         {display:'身份證字號',    
          name:'basic_unicode',    
          width:75,    
          sortable:true,    
          align:'center',    
          hide:false},    
        //第二個欄位    
         {display:'姓名',    
          name:'basic_unicode',    
          width:75,    
          sortable:true,    
          align:'center',    
          hide:false}    
     ],    
    //自訂的參數(以下為範例)    
     params:[    
        //參數1(陣列型)    
         {name:'viewFields',value:['id','basic_unicode','basic_name']},    
        //參數2(單一型)    
         {name:'action',value:'view'}    
        /* 
         附註：另外以下為預設一定會送出去的物件參數，注意不要取同名 
         var param={ 
             page:p.newp, 
             rp:p.rp, 
             sortname:p.sortname, 
             sortorder:p.sortorder, 
             query:p.query, 
             qtype:p.qtype 
         }; 
         */   
/*
     ],    
    //定義功能欄的按鈕資訊(以下為範例)    
     buttons:[    
         {name:'新增',bclass:'add',onpress:null},    
         {name:'刪除',bclass:'delete',onpress:null},    
         {separator:true}    
     ],    
    //定義搜尋欄位資訊(以下為範例)    
     searchitems:[    
         {display:'身份證字號',name:'basic_unicode'},    
         {display:'聯絡手機',name:'contact_mobile'},    
         {display:'姓名',name:'basic_name',isdefault:true}    
     ]    
});
*/


function test(){//query:''
		//$('#flex1').flexOptions({qtype:"admin_id",query:"1"}).flexReload();
		$("#flex1").flexOptions({sortname:"admin_name"}).flexReload();
		return false;
	}

function test2(){//query:''
		$('#flex1').flexOptions({qtype:"admin_id",query:'1'}).flexReload();
		return false;
	}
</script>
<div>
<p>
<a onclick="test2()">test</a>
</p>
</div>


<form id="sform">

<input type="text" name="qtype" id="qtype" value="admin_id" />
<input type="text" name="query" id="query" value="1" />

</form>
</body>
</html>