<script type="text/javascript">

/*
var width = parseInt(screen.width);var height = parseInt(screen.height);
if (width > height) { alert("橫向"); }else { alert("直向"); 

}

var checkOrientation = function(){
         
        mode = Math.abs(window.orientation) == 90 ? 'landscape' : 'portrait';
 
        // 在 Android 上使用 Opera Mobile 測試, 發現要另外這樣判斷
        if ( $.browser.opera )
        {
            width = screen.width;
            height = screen.height;
            mode = width > height ? "landscape" : "portrait";
        }
 
        // 本例為希望在使用者用橫向瀏覽時，就秀出遮罩或警示訊息
        if (mode == 'landscape')
        {
           // 警語遮罩 顯示
           $("#mask").show();
		   
        } else {
           // 警語遮罩 關閉
           $("#mask").hide();
		   
        }
};
 
window.addEventListener("resize", checkOrientation, false);
window.addEventListener("orientationchange", checkOrientation, false);
setInterval(checkOrientation, 500);

$(document).ready(function () {
     $(window)    
          .bind('orientationchange', function(){
               if (window.orientation % 180 == 0){
                   $(document.body).css("-webkit-transform-origin", "")
                       .css("-webkit-transform", "");               
               } 
               else {                   
                   if ( window.orientation > 0) { //clockwise
                     $(document.body).css("-webkit-transform-origin", "200px 190px")
                       .css("-webkit-transform",  "rotate(-90deg)");  
                   }
                   else {
                     $(document.body).css("-webkit-transform-origin", "280px 190px")
                       .css("-webkit-transform",  "rotate(90deg)"); 
                   }
               }
           })
          .trigger('orientationchange'); 
});
*/
function mover(thisv){
		var tit_speed = 500;
		var eng_speed = 400;
				
		var ctitle = '.' + thisv + '_title';
		var cenglish = '.' + thisv + '_english';
		var ch = '#' + thisv + '_h';
		//alert(ctitle + ':' + cenglish + ':' + ch);
		$(ctitle).stop().animate({color:'#FFF'},tit_speed);
		$(cenglish).stop().animate({color:'#FFF'},eng_speed);
		if(ch=="#ch1_h"){
			$('#ch1_h').css({display:"block"}, 0);
			$('#ch1_h').stop().animate({top:"-18px"}, 300);

		}
		if(ch=="#ch2_h"){
			$('#ch2_h').css({display:"block"}, 0);
			$('#ch2_h').stop().animate({top:"-18px"}, 300);
		}
		if(ch=="#ch3_h"){
			$('#ch3_h').css({display:"block"}, 0);
			$('#ch3_h').stop().animate({top:"0px"}, 300);
		}
		if(ch=="#ch4_h"){
			$('#ch4_h').css({display:"block"}, 0);
			$('#ch4_h').stop().animate({top:"-36px"}, 300);
		}
		if(ch=="#ch5_h"){
			$('#ch5_h').css({display:"block"}, 0);
			$('#ch5_h').stop().animate({top:"0px"}, 300);
		}

		
	}
	
	function mout(thisv){
		var tit_speed = 500;
		var eng_speed = 400;
				
		var ctitle = '.' + thisv + '_title';
		var cenglish = '.' + thisv + '_english';
		var ch = '#' + thisv + '_h';

		$(ctitle).stop().animate({color:'#000'},tit_speed);
		$(cenglish).stop().animate({color:'#000'},eng_speed);
		
		$('#ch1_h').stop().animate({top:"70px"}, 300);
		$('#ch2_h').stop().animate({top:"70px"}, 300);
		$('#ch3_h').stop().animate({top:"100px"}, 300);
		$('#ch4_h').stop().animate({top:"70px"}, 300);
		$('#ch5_h').stop().animate({top:"100px"}, 300);

	}




//lightbox
$(document).ready(function(){
	$('.lig_backdrop').click(function(){
					close_box();
	});
});
			
 
  function lightbox(thisv,scontent,ifyn,lgwidth,lgheight,scroll,scrollx){//
				
				var nowwh = GetWebrowser_W_H();
				var nowwhA = nowwh.split(":");
				var nowh = nowwhA[1];
				var noww = nowwhA[0];
				
				var nowwS = (noww - lgwidth) / 2;
				var nowwT = noww - (nowwS*2); 
				if(nowwT<0){nowwT=lgheight;}
				
				
				var nowhS =  (nowh - lgheight) / 2 ;
				var nowhT = nowh - (nowhS*2);
				if(nowhS<0){nowhS=lgheight;}
				$('.lig_backdrop').animate({'opacity':'.50'}, 150, 'linear');
				$('.lig_backdrop').css('display', 'block');
				$('.box').css('width', nowwT);
				$('.box').css('left', nowwS);
				$('.box').css('top', nowhS);
			    $('.box').css('height', nowhT);
				if(scroll=='0'){$('.box').css('overflow', 'hidden');}
				if(scroll=='1'){$('.box').css('overflow', 'auto');}
				if(scrollx=='0'){$('.box').css('overflow-x', 'hidden');}
				
				
	var thisID = '#' + thisv;	
	var cstr = '<div class="close" onclick="LBcolse(\'' + thisv + '\')">x</div>';
	 
	if (ifyn == '0'){//no iframe
			var cstrs = scontent;
			$(thisID).html(cstrs);
		}else{//it's iframe
		
			if (ifyn == '2'){//use iframe
				var instr = '<iframe name="UPiframe" src="' + scontent + '" width="' + lgwidth + '" height="' + lgheight + '" scrolling="auto" frameborder="0" marginheight="0" marginwidth="0"></iframe>';
				$(thisID).html(instr);

			}else{
				$.ajax({
					url:scontent,
					type:"POST",
					dataType: "text",
					data:{},
					success:function(msg){
						//alert(msg);
						//$(thisID).html('');
						var cstrs =msg;
						$(thisID).html(cstrs);	
					}
				});
			}
		}
					 
					 
		$(thisID).animate({'opacity':'.50'}, 150, 'linear');
		$(thisID).animate({'opacity':'1.00'}, 150, 'linear');
		$(thisID).css('display', 'block');
}

 function LBcolse(thisv){
	 
	 var thisID = '#' + thisv;
	 //alert(thisID);
		$(thisID).animate({'opacity':'0'}, 150, 'linear', function(){
		$(thisID).css('display', 'none');
		});	 
		close_box();
}
 
 			function close_box()
			{
				$('.lig_backdrop, .box').animate({'opacity':'0'}, 300, 'linear', function(){
					$('.lig_backdrop, .box').css('display', 'none');
				});
				location.reload() 
			}

</script>

<div id="header">
	
        <a href="index.php"><div class="logo"></div></a>
        
        <div class="choose">
        
           <a href="music_about.php"><div class="ch1" onMouseOver="mover('ch1')" onMouseOut="mout('ch1')">
            	<span class="ch1_title">關於仁友</span>
                <span class="ch1_english">About</span>
				<div class="ch1_h" id="ch1_h"></div></a>
            </div>
            
            
            <a href="music_news.php"><div class="ch2" onMouseOver="mover('ch2')" onMouseOut="mout('ch2')">
            	<span class="ch2_title">最新消息</span>
                <span class="ch2_english">News</span>
                <div class="ch2_h" id="ch2_h"></div></a>
            </div>
            
            <a href="music_product.php"><div class="ch3" onMouseOver="mover('ch3')" onMouseOut="mout('ch3')">
            	<span class="ch3_title">產品情報</span>
                <span class="ch3_english">Product</span>
                <div class="ch3_h" id="ch3_h"></div></a>
            </div>
            
            <a href="music_maintenance.php"><div class="ch4" onMouseOver="mover('ch4')" onMouseOut="mout('ch4')">
            	<span class="ch4_title">維修保養</span>
                <span class="ch4_english">Maintenance</span>
                <div class="ch4_h" id="ch4_h"></div></a>
            </div>
            
            <a href="music_contact.php"><div class="ch5" onMouseOver="mover('ch5')" onMouseOut="mout('ch5')">
            	<span class="ch5_title">線上諮詢</span>
                <span class="ch5_english">Contact</span>
                <div class="ch5_h" id="ch5_h"></div></a>
            </div>
            
        </div>
        <div class="icon_area">

        	<a href="javascript: void(window.open('http://www.plurk.com/?qualifier=shares&status=' .concat(encodeURIComponent(location.href)) .concat(' ') .concat('&#40;') .concat(encodeURIComponent(document.title)) .concat('&#41;')));"><img class="icon" src="images/index/icon1.png" width="20" height="19" onmouseover="this.src='images/index/icon1_h.png'" onmouseout="this.src='images/index/icon1.png'" /></a>
            <a href="javascript: void(window.open('https://plus.google.com/share?url='.concat(encodeURIComponent(location.href)), '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'));"><img class="icon" src="images/index/icon2.png" width="20" height="19" onmouseover="this.src='images/index/icon2_h.png'" onmouseout="this.src='images/index/icon2.png'" /></a>
            <a href="javascript: void(window.open('http://www.facebook.com/share.php?u='.concat(encodeURIComponent(location.href))));"><img class="icon" src="images/index/icon3.png" width="20" height="19" onmouseover="this.src='images/index/icon3_h.png'" onmouseout="this.src='images/index/icon3.png'" /></a>
        </div>
<?
$UserID 	= $_SESSION["UserID"];
$admin_id 	= $_SESSION["admin_id"];

if ($UserID == '' OR $admin_id == ''){
	
?>    
    	<div class="loginarea">
        	<a class="login_in" style="cursor:pointer;" onClick="top.lightbox('targetdiv','mem_login.php','1','600','280','0')">
            	
                	登入 / 
        
            </a>
            
           <a class="login_mem" style="cursor:pointer;" onClick="lightbox('targetdiv','mem_register.php','1','692','420','0')"> 
           		
                	加入會員
              
           </a>
           
        </div>
<?
}else{
?>     
    	<div class="loginareaOK">
        	<a href="logout.php" class="login_in" style="color:red;"> 登出 </a>         
        </div>
<?
}
?>
</div>

