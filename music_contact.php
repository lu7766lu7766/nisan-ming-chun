<?  session_start(); ?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Language" content="zh-tw" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="Description" content="專營建築水電材料製造">
<meta name="Description" content="銘浚興業設立於雲林縣，專營建築水電材料製造，期能提供更好的服務、品質及低價格供應大眾。產品項目：PVC管零件製造、不銹鋼放衣架、立布生產、PVC銅珠球塞、各種水電材料批發、雲林PVC塑膠接頭零件系列、PVC銅珠凡而、PVC凡而、套銅外牙接頭零件、套銅龍口接頭系列、排水管帽系列、手工大月彎、PVC法蘭接頭、水塔接頭、ABS馬達架、不銹鋼立布、竹筏塞頭、TP-636活動放衣架">
<meta name="KeyWords" content="銘浚興業設立於雲林縣，專營建築水電材料製造，期能提供更好的服務、品質及低價格供應大眾。產品項目：PVC管零件製造、不銹鋼放衣架、立布生產、PVC銅珠球塞、各種水電材料批發、雲林PVC塑膠接頭零件系列、PVC銅珠凡而、PVC凡而、套銅外牙接頭零件、套銅龍口接頭系列、排水管帽系列、手工大月彎、PVC法蘭接頭、水塔接頭、ABS馬達架、不銹鋼立布、竹筏塞頭、TP-636活動放衣架">
<title>銘浚興業有限公司--線上諮詢</title>
<link rel="stylesheet" href="CSS/music_style.css" type="text/css">
<meta name="Author" content="FLYINGANGLE 飛角設計">
<link rev="made" href="fastudio268@gmail.com">
<link rev="made" href="http://www.fas-d.com/">

<meta property="og:description" content="銘浚興業設立於雲林縣，專營建築水電材料製造，期能提供更好的服務、品質及低價格供應大眾。產品項目：PVC管零件製造、不銹鋼放衣架、立布生產、PVC銅珠球塞、各種水電材料批發、雲林PVC塑膠接頭零件系列、PVC銅珠凡而、PVC凡而、套銅外牙接頭零件、套銅龍口接頭系列、排水管帽系列、手工大月彎、PVC法蘭接頭、水塔接頭、ABS馬達架、不銹鋼立布、竹筏塞頭、TP-636活動放衣架" />
<meta property="og:title" content="銘浚興業有限公司---專營建築水電材料製造"/>
<meta property="og:type" content="website"/>
<meta property="og:url" content="http://www.fas-d.com/music/music.php"/>
<meta property="og:image" content="http://www.fas-d.com/music/images/index/index_bg.jpg"/>
<meta property="og:image" content="http://www.fas-d.com/music/images/about/about_bg.jpg"/>
<meta property="og:site_name" content="銘浚興業有限公司---臺中古典樂器推手" />


<script type="text/javascript" src="http://maps.google.com/maps/api/js?libraries=geometry&sensor=false"></script>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<link rel="stylesheet" href="CSS/music_style.css" type="text/css">
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery.color.js"></script>
<script type="text/javascript">

 $(document).ready(function(){
/*  var latlng = new google.maps.LatLng(24.127916,120.681764);
  //變數是小寫latlng， new後面的LatLng 的L是大寫!!! 
  var myOptions = {
    zoom: 15,
    center: latlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  //建立google.maps.Map - 基本物件，定義網頁上的單一地圖
  var map = new google.maps.Map(document.getElementById("mymap"),myOptions);*/
   var myLatlng = new google.maps.LatLng(23.7520608,120.5699107);
  var mapOptions = {
    zoom: 15,
    center: myLatlng
  };

  var map = new google.maps.Map(document.getElementById('mymap'), mapOptions);

  var contentString = '<div id="content">'+
      '<div id="siteNotice">'+
      '</div>'+
      '<h3 id="firstHeading" class="firstHeading">銘浚興業有限公司---專營建築水電材料製造</h3>'+
      '<div id="bodyContent" style="max-width:400px; height:100px;"> 銘浚興業設立於雲林縣，專營建築水電材料製造，期能提供更好的服務、品質及低價格供應大眾。產品項目：PVC管零件製造、不銹鋼放衣架、立布生產、PVC銅珠球塞、各種水電材料批發、雲林PVC塑膠接頭零件系列、PVC銅珠凡而、PVC凡而、套銅外牙接頭零件、套銅龍口接頭系列、排水管帽系列、手工大月彎、PVC法蘭接頭、水塔接頭、ABS馬達架、不銹鋼立布、竹筏塞頭、TP-636活動放衣架'+
      '</div>'+
      '</div>';

  var infowindow = new google.maps.InfoWindow({
      content: contentString
  });

  var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title: 'Uluru (Ayers Rock)'
  });
  google.maps.event.addListener(marker, 'click', function() {
    infowindow.open(map,marker);
  });

  
 });
 
//擷取螢幕寬高	
function GetWebrowser_W_H(thisv)

{

var myWidth;
var myHeight;

if( typeof( window.innerWidth ) == 'number' ) { 

//用在不是IE的瀏覽器上

myWidth = window.innerWidth;
myHeight = window.innerHeight; 

} else if( document.documentElement && 

( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) { 

//用在IE6以上

myWidth = document.documentElement.clientWidth; 
myHeight = document.documentElement.clientHeight; 

} else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) { 

//相容IE4

myWidth = document.body.clientWidth; 
myHeight = document.body.clientHeight; 

}

/*
	if (thisv == 'nowh'){
		return myHeight;
	}else{
		return myWidth;
	}
*/	
	return myWidth + ':' + myHeight;

}




$(document).ready(function() {

	/*
	$('.ch1').mouseover(function(){
		
		$('.ch1_title').stop().animate({color:'#FFF'},tit_speed);
		$('.ch1_english').stop().animate({color:'#FFF'},eng_speed);
		$('.ch1_h').stop().animate({top:"-21px"}, 300);

		
	});
	$('.ch1').mouseout(function(){
		$('.ch1_title').stop().animate({color:'#000'},tit_speed);
		$('.ch1_english').stop().animate({color:'#000'},eng_speed);
		$('.ch1_h').stop().animate({top:"279px"}, 300);
	});
	*/
	//$('#ch1_cc').addClass('animated fadeInUp');
	
	//首頁動畫把PX轉到0
	$('.contact_line').stop().animate({width:"0px"}, 0);
	$('.contact_txt').stop().animate({width:"0px"}, 0);
	//$('.about_txt2').stop().animate({width:"0px"}, 0);
	//$('.about_txt3').stop().animate({width:"0px"}, 0);
	
	//首頁選單把PX轉到0
	/*$('.ch1_h').stop().animate({width:"0px"}, 0);
	$('.ch2_h').stop().animate({width:"0px"}, 0);
	$('.ch3_h').stop().animate({width:"0px"}, 0);
	$('.ch4_h').stop().animate({width:"0px"}, 0);
	$('.ch5_h').stop().animate({width:"0px"}, 0);*/
	
	//$('.main_txt1').stop().animate({width:"162px"}, 1000);
	setTimeout("contact_line()",1000);
	
}); 

function contact_line()
{
	//alert();
	$('.contact_line').stop().animate({width:"125px"}, 1000);	
	setTimeout("contact_txt()",1000);
}

function contact_txt()
{
	$('.contact_txt').stop().animate({width:"229px"}, 1000);	
	//setTimeout("about_txt2()",1000);
}

function about_txt2()
{
	$('.about_txt2').stop().animate({width:"140px"}, 1000);	
	setTimeout("about_txt3()",1000);
}

function about_txt3()
{
	$('.about_txt3').stop().animate({width:"152px"}, 1000);
}

function checkinput()
{
	var name = $("#name").val();	
	var tel = $("#tel").val();	
	var mailA = $("#mailA").val();	
	var content = $("#content").val();	
	
	if(name != "" && mailA != "" && content != "")
	{
		$('#send').submit();		
	}
}
	
function clearinput()
{
	$("#content").val(''); 
	$("#name").val('');	
	$("#tel").val('');
	$("#mailA").val('');

}

</script>

</head>

<body>
<? include("music_part/top.php");?>

<!--oooooooooooooooooooooooooooooooo-->

<div id="mymap"></div>
<div id = "contact_inter">
	<div class="contact_line"></div>
    <div class="contact_txt"></div>  
    <div class="contact_textarea"> 
    <form id="send" name="send" action="music_sendmail.php" method="POST" >
        <div class="contact_text1"><input id="name" name="name" style="width:260px;" type="text" size="42" placeholder="姓名:"></div>
        <div class="contact_text2"><input id="tel" name="tel" style="width:260px;" type="text" size="42" placeholder="電話:"></div>
        <div class="contact_text3"><input id="mailA" name="mailA" style="width:260px;" type="text" size="42" placeholder="E-Mail:"></div>
        <div class="contact_text4"><textarea style="width:260px; height:120px; resize: none;" name="content" id="content" cols="32" rows="5" placeholder="內容:"></textarea></div> 
        <div class="contact_btn1" onClick="clearinput()"></div>
        <div class="contact_btn2" onClick="checkinput()"></div>
    </form>    
    </div>
</div>


<!--oooooooooooooooooooooooooooooooo-->

<div class="lig_backdrop"></div>
<div class="box" id="targetdiv"></div>
<div class="box" id="targetdiv2"></div>
<? include("music_part/footer.php");?>

</body>
</html>