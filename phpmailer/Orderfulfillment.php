
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>訂單完成通知函</title>

<table width="660" border="0">
  <tr>
    <td><span class="style1"><span class="style1-1">LOHA樂活優達購物</span> ─ 訂單完成通知函！</span></td>
  </tr>
  <tr>
  <td><span class="style2"></span></td>
  </tr>
</table>

<table width="660" border="0" class="tableb1">
  <tr>
     <td class="style5">《LOHA樂活優達購物》訂 單 通 知 信</td>
  </tr>
  
  <tr>
     <td width="660" height="20"></td>
  </tr>
  
  <tr>
    <td class="style4">親愛的<span class="style4-1">劉德華</span>先生/小姐您好： </td>
  </tr>
  
  <tr>
    <td class="style5">感謝您向《LOHA樂活優達購物》訂購商品，我們已經收到您的訂購資訊！</td>
  </tr>
  <tr>
  	<td class="style5">本通知函只是通知您本系統已經收到您的訂購訊息並供您再次自行核對之用，不代表交易已經完成。</td>
  </tr>
  <tr>
     <td width="660" height="20"></td>
  </tr>
  <tr>
    <td class="style3">提醒您！</td>
  </tr>
    <tr>
	<td class="style3">本網站不會以電話通知更改付款方式或要求改以ATM重新轉帳。</td>
  </tr>
    <tr>
	<td class="style3">亦不會委託廠商以電話通知變更付款方式或要求提供ATM匯款帳號。</td>
  </tr>
    <tr>
     <td width="660" height="20"></td>
  </tr>
      <tr>
     <td class="style3">您的訂單資訊如下</td>
  </tr>

</table>
  
  

  
  
 
 <table width="650" class="tableb" align="center" cellspacing="0">
   <tr>
     <td class="tableright">訂 單 編 號</td>
     <td class="tabledown">
	 	<span>YYYY</span>
		<span>MM</span>
		<span>DD</span>
		<span>12345</span>	 </td>
   </tr>
   <tr>
     <td class="tableright1">訂 購 日 期</td>
     <td class="tabledown1">
	 	<span>YYYY</span>
		<span>年</span>
		<span>MM</span>
		<span>月</span>
		<span>DD</span>
		<span>日</span>
		<span>HH</span>
		<span>時</span>
		<span>MM</span>
		<span>分</span>	 </td>
   </tr>
   <tr>
     <td class="tableright2">訂 單 明 細</td>
     <td class="tabledown2" >我們將會在最短時間內處理您的訂單！
	 <br />&nbsp;訂單明細請至『<span><a href="#">我的帳戶</a></span>』進行訂單查詢</td>
   </tr>
   <tr>
     <td valign="middle" class="tableright1">收 貨 人 資 料</td>
     <td class="tabledown1">請至『<span><a href="#">我的帳戶</a></span>』查詢</td>
   </tr>
   <tr>
     <td colspan="2" class="tableright3">注 意 事 項：</td>
   </tr>
   <tr>
     <td class="tableright4">訂單作業時間</td>
     <td class="tabledown4">
	 	<li>若您是使用ATM轉帳付款，請於訂購日起三日內轉帳，欲查詢ATM轉帳帳號及轉帳
			<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;請至『<span><a href="#">我的帳戶</a></span>』查詢訂單。</li> 
		<li>如果您所訂購的商品仍有存貨，《LOHA樂活優達購物》在確認收款及交易條件無
			<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;誤後，會即刻通知廠商送貨。由於不同商品的送貨時間會有些許不同，請您參照網
			<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;站上商品說明頁。</li> 
		<li>出貨廠商會在２個工作天內確認出貨，如果您所訂購的商品缺貨，《LOHA樂活優
			<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;達購物》會立即通知您並取消訂單。</li> 

	 
	 </td>
   </tr>
   <tr>
     <td class="tableright2">訂 單 查 詢</td>
     <td class="tabledown2">
	 	您可至『<span><a href="#">我的帳戶</a></span>』查詢。
		<br />&nbsp;若對訂單有疑問也可提出詢問，我們將在1~2日內 ( 不含週六日 ) 儘速email回覆您！

	 </td>
   </tr>
   <tr>
     <td class="tableright5">取 貨 需 知</td>
     <td class="tabledown5">
	 	<li>若您是使用至門市取貨方式，當商品送達指定門市後將以E-mail方式通知您，請於
		<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;取貨時間內前往該門市取貨。
		<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;欲查詢或列印您的繳款代碼和金額，請至『<span><a href="#">我的帳戶</a></span>』。</li> 
		<li>一般商品將於付款成功並確認交易條件無誤且有庫存後的七個工作天內送達 ( 特殊
		<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;商品則依網頁說明時間出貨 )。 </li>

	 </td>
   </tr>
   <tr>
     <td class="tableright2">其　　　他</td>
     <td class="tabledown2">
	 	請注意，您訂購之商品若經配送兩次無法送達，再經本公司以電話與email均無法聯繫
		<br />&nbsp;逾三天者，本公司將取消該筆訂單，並且全額退款。
	 </td>
   </tr>
 </table>
 
 <table>
     <tr>
     <td width="660" height="20"></td>
  </tr>
  <tr>
    <td class="style31">
	※此信件為系統自動寄出，請勿直接回覆。若您有訂單方面問題請至網站「聯絡我們」提
	<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;出，謝謝！權利。</td>
  </tr>

  <tr>
  <td><span class="style2-1"></span></td>
  </tr>
    <tr>
     <td width="660" height="20"></td>
  </tr>
     <tr>
    <td class="style5-1">LOHA樂活優達，祝您生活更樂活～</td>
  </tr>
     <tr>
    <td class="style5-1">LOHA樂活優達購物：<a href="http://www.lohashop.com.tw/k/index.php">http://www.lohashop.com.tw/k/index.php</a></td>
  </tr>
</table>


