<?  session_start(); ?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="Description" content="專營建築水電材料製造">
<meta name="Description" content="專業而卓越的售後服務--工欲善其事，必先利其器，樂器是為音樂藝術家最重要的夥伴， 銘浚興業有限公司提供各式樂器維修保養服務，讓各式音樂演奏能優美而長久。">
<meta name="KeyWords" content="銘浚興業設立於雲林縣，專營建築水電材料製造，期能提供更好的服務、品質及低價格供應大眾。產品項目：PVC管零件製造、不銹鋼放衣架、立布生產、PVC銅珠球塞、各種水電材料批發、雲林PVC塑膠接頭零件系列、PVC銅珠凡而、PVC凡而、套銅外牙接頭零件、套銅龍口接頭系列、排水管帽系列、手工大月彎、PVC法蘭接頭、水塔接頭、ABS馬達架、不銹鋼立布、竹筏塞頭、TP-636活動放衣架">
<title>銘浚興業有限公司--維修保養</title>
<link rel="stylesheet" href="CSS/music_style.css" type="text/css">
<meta name="Author" content="FLYINGANGLE 飛角設計">
<link rev="made" href="fastudio268@gmail.com">
<link rev="made" href="http://www.fas-d.com/">

<meta content="專業而卓越的售後服務--工欲善其事，必先利其器，樂器是為音樂藝術家最重要的夥伴， 銘浚興業有限公司提供各式樂器維修保養服務，讓各式音樂演奏能優美而長久。" property="og:description" />
<meta property="og:title" content="銘浚興業有限公司---臺中古典樂器推手"/>
<meta property="og:type" content="website"/>
<meta property="og:url" content="http://www.fas-d.com/music/music.php"/>
<meta property="og:image" content="http://www.fas-d.com/music/images/index/index_bg.jpg"/>
<meta property="og:image" content="http://www.fas-d.com/music/images/about/about_bg.jpg"/>
<meta property="og:site_name" content="銘浚興業有限公司---臺中古典樂器推手" />

<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery.color.js"></script>
<script type="text/javascript" src="js/jquery.cycle.all.js"></script>
<link rel="stylesheet" href="CSS/music_style.css" type="text/css">

<script type="text/javascript">


//擷取螢幕寬高	
function GetWebrowser_W_H(thisv)

{

var myWidth;
var myHeight;

if( typeof( window.innerWidth ) == 'number' ) { 

//用在不是IE的瀏覽器上

myWidth = window.innerWidth;
myHeight = window.innerHeight; 

} else if( document.documentElement && 

( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) { 

//用在IE6以上

myWidth = document.documentElement.clientWidth; 
myHeight = document.documentElement.clientHeight; 

} else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) { 

//相容IE4

myWidth = document.body.clientWidth; 
myHeight = document.body.clientHeight; 

}

/*
	if (thisv == 'nowh'){
		return myHeight;
	}else{
		return myWidth;
	}
*/	
	return myWidth + ':' + myHeight;

}





$(document).ready(function() {

	/*
	$('.ch1').mouseover(function(){
		
		$('.ch1_title').stop().animate({color:'#FFF'},tit_speed);
		$('.ch1_english').stop().animate({color:'#FFF'},eng_speed);
		$('.ch1_h').stop().animate({top:"-21px"}, 300);

		
	});
	$('.ch1').mouseout(function(){
		$('.ch1_title').stop().animate({color:'#000'},tit_speed);
		$('.ch1_english').stop().animate({color:'#000'},eng_speed);
		$('.ch1_h').stop().animate({top:"279px"}, 300);
	});
	*/
	//$('#ch1_cc').addClass('animated fadeInUp');
	
	//首頁動畫把PX轉到0
	$('.about_line').stop().animate({width:"0px"}, 0);
	$('.maintenance_txt1').stop().animate({width:"0px"}, 0);
	$('.maintenance_txt2').stop().animate({width:"0px"}, 0);
	$('.maintenance_txt3').stop().animate({width:"0px"}, 0);
	
	//首頁選單把PX轉到0
	/*$('.ch1_h').stop().animate({width:"0px"}, 0);
	$('.ch2_h').stop().animate({width:"0px"}, 0);
	$('.ch3_h').stop().animate({width:"0px"}, 0);
	$('.ch4_h').stop().animate({width:"0px"}, 0);
	$('.ch5_h').stop().animate({width:"0px"}, 0);*/
	
	//$('.main_txt1').stop().animate({width:"162px"}, 1000);
	setTimeout("about_line()",1000);
	
}); 

function about_line()
{
	//alert();
	$('.about_line').stop().animate({width:"100px"}, 1000);	
	setTimeout("maintenance_txt1()",1000);
}

function maintenance_txt1()
{
	$('.maintenance_txt1').stop().animate({width:"81px"}, 1000);	
	setTimeout("maintenance_txt2()",1000);
}

function maintenance_txt2()
{
	$('.maintenance_txt2').stop().animate({width:"261px"}, 1000);	
	setTimeout("maintenance_txt3()",1000);
}

function maintenance_txt3()
{
	$('.maintenance_txt3').stop().animate({width:"158px"}, 1000);
}

</script>

</head>

<body>
<? include("music_part/top.php");?>

<!--oooooooooooooooooooooooooooooooo-->

<div id="main">

	<div class="maintenance_img"></div>
    <div class="about_line main_level"></div>
    <div class="about_main">
        
        <div class="maintenance_txt1 main_level"></div>
        <div class="maintenance_txt2 main_level"></div>
        <div class="maintenance_txt3 main_level"></div>
    
        <div class="about_intro main_level">
            <font style="font-weight:bold">專業而卓越的售後服務</font><br><br>
           工欲善其事，必先利其器，樂器是為音樂藝術家最重要的夥伴，
		   銘浚興業有限公司提供各式樂器維修保養服務，讓各式音樂演奏能優美而長久。<br><br>
            
            <font style="font-weight:bold">
            	樂器維修請來店預約<br>
				Tel: 04-2285-8556&nbsp&nbsp&nbspFax：04-2285-6787<br>
				E-mail：jenyou2285@gmail.com
            </font>
            
        </div>
	</div>
</div>


<!--oooooooooooooooooooooooooooooooo-->

<div class="lig_backdrop"></div>
<div class="box" id="targetdiv"></div>
<div class="box" id="targetdiv2"></div>
<? include("music_part/footer.php");?>

</body>
</html>